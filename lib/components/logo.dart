import 'package:flutter/cupertino.dart';

class Logo {
  Widget build(double width, double height, String target) => Container(
        width: width,
        height: height,
        decoration:
            BoxDecoration(image: DecorationImage(image: AssetImage(target))),
      );
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/utils/component.dart';

class Sheet {
  void build(BuildContext context, String icon, String message) {
    showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          return Center(
              child: Wrap(children: [
            Center(
              child: Dialog(
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      logo.build(64.0, 64.0, icon),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        message,
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
              ),
            )
          ]));
        });
  }

  void buildBottom(BuildContext context, List<Widget> content) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (context) {
          return SafeArea(child: LayoutBuilder(
            builder: (context, constraints) {
              return Padding(
                  padding: MediaQuery.of(context).viewInsets,
                  child: SingleChildScrollView(
                    child: Wrap(children: content),
                  ));
            },
          ));
        });
  }
}

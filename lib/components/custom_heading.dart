import 'package:flutter/cupertino.dart';
import 'package:jmtopay/helper/tint.dart';

class Heading extends StatelessWidget {
  final String label;

  Heading({this.label});

  @override
  Widget build(BuildContext context) {
    return Text(
      this.label,
      style: TextStyle(
          color: Tint.heading, fontWeight: FontWeight.bold, fontSize: 24),
      textAlign: TextAlign.center,
    );
  }
}

class SubHeading extends StatelessWidget {
  final String label;

  SubHeading({this.label});

  @override
  Widget build(BuildContext context) {
    return Text(
      this.label,
      style: TextStyle(
          color: Tint.heading, fontWeight: FontWeight.bold, fontSize: 18),
      textAlign: TextAlign.start,
    );
  }
}

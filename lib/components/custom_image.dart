import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ImageNetwork extends StatelessWidget {
  final double width, height;
  final String url;

  ImageNetwork(
      {@required this.width, @required this.height, @required this.url});

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: this.url,
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.cover,
          ),
        ),
        height: this.height,
        width: this.width,
      ),
      errorWidget: (context, url, error) => Icon(
        Icons.error,
        color: Colors.red,
      ),
    );
  }
}

class ImageSet extends StatelessWidget {
  final double width, height;
  final String url;

  ImageSet({@required this.width, @required this.height, @required this.url});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(this.url),
          fit: BoxFit.cover,
        ),
      ),
      height: this.height,
      width: this.width,
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:jmtopay/helper/tint.dart';

class Description extends StatelessWidget {
  final String label;

  Description({this.label});

  @override
  Widget build(BuildContext context) {
    return Text(
      this.label,
      style: TextStyle(color: Tint.text, fontSize: 14),
      textAlign: TextAlign.center,
    );
  }
}

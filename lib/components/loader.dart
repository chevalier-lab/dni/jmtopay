import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/helper/str.dart';

class Loader {
  void build(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext dialogContext) {
          return Center(
              child: Wrap(children: [
            Center(
              child: Dialog(
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        STR.loadingMessage,
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
              ),
            )
          ]));
        });
  }
}

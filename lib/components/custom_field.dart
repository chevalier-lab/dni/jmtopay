import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jmtopay/helper/tint.dart';

class Field extends StatelessWidget {
  final Function(String) onTyping;
  final dynamic field;
  final bool isAvalid;

  Field(
      {@required this.field, @required this.isAvalid, @required this.onTyping});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(field["label"],
            textAlign: TextAlign.start, style: TextStyle(color: Tint.label)),
        SizedBox(height: 8),
        TextField(
          onChanged: onTyping,
          style: TextStyle(
            color: Tint.field,
          ),
          keyboardType: field["type"],
          inputFormatters: <TextInputFormatter>[field["format"]],
          decoration: InputDecoration(
            prefixIcon: Icon(field["icon"], color: Tint.hint),
            suffixIcon: (this.isAvalid)
                ? Icon(Icons.check_circle_outline_outlined, color: Tint.success)
                : Icon(Icons.check_circle_outline_outlined,
                    color: Tint.disabled),
            hintText: field["hint"],
            fillColor: Tint.light,
            hintStyle: TextStyle(
              color: Tint.hint,
            ),
            filled: true,
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
            disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
          ),
        )
      ],
    );
  }
}

class FieldPassword extends StatelessWidget {
  final Function(String) onTyping;
  final dynamic field;
  final bool isAvalid;

  FieldPassword(
      {@required this.field, @required this.isAvalid, @required this.onTyping});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(field["label"],
            textAlign: TextAlign.start, style: TextStyle(color: Tint.label)),
        SizedBox(height: 8),
        TextField(
          onChanged: onTyping,
          style: TextStyle(
            color: Tint.field,
          ),
          obscureText: true,
          keyboardType: field["type"],
          inputFormatters: <TextInputFormatter>[field["format"]],
          decoration: InputDecoration(
            prefixIcon: Icon(field["icon"], color: Tint.hint),
            suffixIcon: (this.isAvalid)
                ? Icon(Icons.check_circle_outline_outlined, color: Tint.success)
                : Icon(Icons.check_circle_outline_outlined,
                    color: Tint.disabled),
            hintText: field["hint"],
            fillColor: Tint.light,
            hintStyle: TextStyle(
              color: Tint.hint,
            ),
            filled: true,
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
            disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
          ),
        )
      ],
    );
  }
}

class FieldControl extends StatelessWidget {
  final Function(String) onTyping;
  final dynamic field;
  final bool isAvalid;

  FieldControl(
      {@required this.field, @required this.isAvalid, @required this.onTyping});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(field["label"],
            textAlign: TextAlign.start, style: TextStyle(color: Tint.label)),
        SizedBox(height: 8),
        TextField(
          onChanged: onTyping,
          style: TextStyle(
            color: Tint.field,
          ),
          maxLength: field["max_length"],
          controller: field["controller"],
          keyboardType: field["type"],
          inputFormatters: <TextInputFormatter>[field["format"]],
          decoration: InputDecoration(
            counterText: '',
            prefixIcon: Icon(field["icon"], color: Tint.hint),
            suffixIcon: (this.isAvalid)
                ? Icon(Icons.check_circle_outline_outlined, color: Tint.success)
                : Icon(Icons.check_circle_outline_outlined,
                    color: Tint.disabled),
            hintText: field["hint"],
            fillColor: Tint.light,
            hintStyle: TextStyle(
              color: Tint.hint,
            ),
            filled: true,
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
            disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
          ),
        )
      ],
    );
  }
}

class FieldSearch extends StatelessWidget {
  final Function(String) onTyping;
  final VoidCallback onClear;
  final dynamic field;
  final bool isAvalid;

  FieldSearch(
      {@required this.field,
      @required this.isAvalid,
      @required this.onTyping,
      @required this.onClear});

  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: onTyping,
      controller: field["controller"],
      style: TextStyle(
        color: Tint.field,
      ),
      keyboardType: field["type"],
      inputFormatters: <TextInputFormatter>[field["format"]],
      decoration: InputDecoration(
        prefixIcon: Icon(field["icon"], color: Tint.hint),
        suffixIcon: (this.isAvalid)
            ? GestureDetector(
                child: Icon(Icons.clear, color: Tint.danger),
                onTap: this.onClear)
            : Icon(Icons.clear, color: Tint.disabled),
        hintText: field["hint"],
        fillColor: Tint.white,
        hintStyle: TextStyle(
          color: Tint.hint,
        ),
        filled: true,
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Tint.light, width: 2.0),
          borderRadius: BorderRadius.circular(16),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Tint.light, width: 2.0),
          borderRadius: BorderRadius.circular(16),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Tint.light, width: 2.0),
          borderRadius: BorderRadius.circular(16),
        ),
        disabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Tint.light, width: 2.0),
          borderRadius: BorderRadius.circular(16),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Tint.light, width: 2.0),
          borderRadius: BorderRadius.circular(16),
        ),
      ),
    );
  }
}

class FieldPIN extends StatelessWidget {
  final Function(String) onTyping;
  final dynamic field;
  final bool isAvalid;

  FieldPIN(
      {@required this.field, @required this.isAvalid, @required this.onTyping});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextField(
        onChanged: onTyping,
        maxLength: 1,
        style: TextStyle(
          color: Tint.field,
          fontSize: 20,
        ),
        textAlign: TextAlign.center,
        keyboardType: field["type"],
        inputFormatters: <TextInputFormatter>[field["format"]],
        focusNode: field["focusNode"],
        decoration: InputDecoration(
          counterText: '',
          contentPadding: EdgeInsets.symmetric(vertical: 16),
          border: UnderlineInputBorder(
            borderSide: BorderSide(color: Tint.main, width: 2.0),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Tint.main, width: 2.0),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Tint.main, width: 2.0),
          ),
          disabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Tint.main, width: 2.0),
          ),
          focusedErrorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Tint.main, width: 2.0),
          ),
        ),
      ),
      padding: EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
          color: Tint.light, borderRadius: BorderRadius.circular(16)),
    );
  }
}

class Textarea extends StatelessWidget {
  final Function(String) onTyping;
  final dynamic field;

  Textarea({@required this.field, @required this.onTyping});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 8),
        TextField(
          onChanged: onTyping,
          style: TextStyle(
            color: Tint.field,
          ),
          keyboardType: field["type"],
          inputFormatters: <TextInputFormatter>[field["format"]],
          maxLines: 5,
          decoration: InputDecoration(
            hintText: field["hint"],
            fillColor: Tint.light,
            hintStyle: TextStyle(
              color: Tint.hint,
            ),
            filled: true,
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
            disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Tint.light, width: 2.0),
              borderRadius: BorderRadius.circular(16),
            ),
          ),
        )
      ],
    );
  }
}

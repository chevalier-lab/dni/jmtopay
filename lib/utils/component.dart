import 'package:jmtopay/components/loader.dart';
import 'package:jmtopay/components/logo.dart';
import 'package:jmtopay/components/sheet.dart';

final Logo logo = Logo();
final Sheet sheet = Sheet();
final Loader loader = Loader();

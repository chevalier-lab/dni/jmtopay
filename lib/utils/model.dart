import 'dart:convert';

import 'package:dio/dio.dart';

class Models {
  Dio initial(baseURL) {
    // Setting Dio
    BaseOptions options = new BaseOptions(
      baseUrl: baseURL,
      connectTimeout: 0,
      receiveTimeout: 0,
    );
    Dio dio = new Dio(options);
    return dio;
  }

  // Get
  Future<Response> getData(dio, String path, {Options option}) async {
    if (option == null)
      return await dio.get(path);
    else
      return await dio.get(path, options: option);
  }

  // Post Form Data
  Future<Response> formData(dio, String path, Map<String, dynamic> body,
      {Options option}) async {
    if (option == null)
      return await dio.post(path, data: FormData.fromMap(body));
    else
      return await dio.post(path,
          data: FormData.fromMap(body), options: option);
  }

  // Post Form URL Encoded
  Future<Response> formURLEncoded(dio, String path, Map<String, dynamic> body,
      {Options option}) async {
    if (option == null)
      return await dio.post(path,
          data: body,
          options: Options(contentType: Headers.formUrlEncodedContentType));
    else
      return await dio.post(path, data: body, options: option);
  }

  // Post Form URL Encoded
  Future<Response> raw(dio, String path, Map<String, dynamic> body,
      {Options option}) async {
    if (option == null)
      return await dio.post(path, data: jsonEncode(body));
    else
      return await dio.post(path, data: jsonEncode(body), options: option);
  }
}

import 'package:jmtopay/apps/auth/provider.dart';
import 'package:jmtopay/apps/home/provider.dart';
import 'package:jmtopay/apps/splashscreen/provider.dart';

final initialRoute = SplashScreenProvider.routeName;
final routeBridge = {
  SplashScreenProvider.routeName: (context) => SplashScreenProvider.init(),
  AuthProvider.routeName: (context) => AuthProvider.init(),
  HomeProvider.routeName: (context) => HomeProvider.init()
};

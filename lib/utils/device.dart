import 'dart:io';

import 'package:device_info/device_info.dart';

class Device {
  Future<String> getID() async {
    var deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      var infoDevice = await deviceInfo.iosInfo;
      return infoDevice.identifierForVendor;
    } else {
      var infoDevice = await deviceInfo.androidInfo;
      return infoDevice.androidId;
    }
  }
}

import 'package:geolocation/geolocation.dart';

class Location {
  void getCurrentLocation(Function(dynamic) onSuccess) async {
    Geolocation.enableLocationServices().then((result) {
      // Request Location Services
      print("SUCCESS_REQUEST_LOCATION_SERVICES");
      print(result);
      setupListener((latLng) => onSuccess(latLng));
    }).catchError((e) {
      print("ERROR_REQUEST_LOCATION_SERVICES");
      print(e);
    });
  }

  void setupListener(Function(dynamic) latLng) {
    Geolocation.currentLocation(accuracy: LocationAccuracy.best)
        .listen((result) {
      print("SUCCESS_REQUEST_LOCATION");
      if (result.isSuccessful)
        latLng({
          "lat": result.location.latitude.toString(),
          "lng": result.location.longitude.toString()
        });
    }).onError((e) {
      print("ERROR_REQUEST_LOCATION");
      print(e);
    });
  }
}

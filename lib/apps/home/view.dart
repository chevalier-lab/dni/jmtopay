import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/home/pages/link_card.dart';
import 'package:jmtopay/apps/home/pages/new_card.dart';
import 'package:jmtopay/apps/home/pages/wallet.dart';
import 'package:jmtopay/models/home_md.dart';
import 'package:provider/provider.dart';

class HomeView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomeView();
}

class _HomeView extends State<HomeView> {
  @override
  void initState() {
    super.initState();
  }

  Widget setupPage(HomeMD provider) {
    var page = provider.currentPage;

    switch (page) {
      case WalletPageProvider.pageName:
        return WalletPageProvider.init(context);
      case NewCardPageProvider.pageName:
        return NewCardPageProvider.init(context);
      default:
        return LinkCardPageProvider.init(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<HomeMD>(context);
    return setupPage(provider);
  }
}

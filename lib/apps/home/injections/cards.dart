import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:jmtopay/api/cards/bindings.dart';
import 'package:jmtopay/helper/global.dart';
import 'package:jmtopay/helper/request.dart';
import 'package:jmtopay/helper/sheet.dart';
import 'package:jmtopay/helper/url.dart';
import 'package:jmtopay/utils/component.dart';

class CardInjections {
  Dio dio;
  RequestModel requestModel;
  BindingsController controller;
  BuildContext context;

  // Setup Initialize
  void initialize(BuildContext context) {
    this.context = context;
    this.dio = models.initial(BaseURLAPI.uri);
    this.requestModel = RequestModel(dio: dio);

    this.controller =
        BindingsController(context: context, requestModel: this.requestModel);
  }

  // Check Binding
  void checkBinding(Function(List<dynamic>) onSuccess) {
    controller.check((data) {
      onSuccess(data);
    }, (message) {
      checkConnection((status) {
        if (status != "Connect")
          error(context, "Tidak terhubung ke internet.");
        else
          error(context, message);
      });
    });
  }

  // Do Binding
  void doBinding(String cardPan, String deviceID, String latitude,
      String longitude, String expDate, VoidCallback onSuccess) {
    loader.build(context);
    controller.register({
      RequestKey.cardPan: cardPan,
      RequestKey.deviceID: deviceID,
      RequestKey.latitude: latitude,
      RequestKey.longitude: longitude,
      RequestKey.expDate: expDate
    }, (data) {
      Navigator.of(context).pop();
      onSuccess();
    }, (message) {
      Navigator.of(context).pop();
      checkConnection((status) {
        if (status != "Connect")
          error(context, "Tidak terhubung ke internet.");
        else
          error(context, message);
      });
    });
  }

  // Do Binding Link Aja
  void doBindingLinkAja(String phone, String deviceID, String latitude,
      String longitude, Function(dynamic) onSuccess) {
    loader.build(context);
    controller.registerLinkAja({
      RequestKey.phone: phone,
      RequestKey.deviceID: deviceID,
      RequestKey.latitude: latitude,
      RequestKey.longitude: longitude
    }, (data) {
      Navigator.of(context).pop();
      onSuccess(data);
    }, (message) {
      Navigator.of(context).pop();
      checkConnection((status) {
        if (status != "Connect")
          error(context, "Tidak terhubung ke internet.");
        else
          error(context, message);
      });
    });
  }

  // Do Verify
  void doVerify(String passcode, String cardPan, VoidCallback onSuccess) {
    loader.build(context);
    controller.verify(
        {RequestKey.passcode: passcode, RequestKey.cardPan: cardPan}, (data) {
      Navigator.of(context).pop();
      onSuccess();
    }, (message) {
      Navigator.of(context).pop();
      checkConnection((status) {
        if (status != "Connect")
          error(context, "Tidak terhubung ke internet.");
        else
          error(context, message);
      });
    });
  }

  // Do UnBinding
  void doUnBinding(String cardPan, String phone, VoidCallback onSuccess) {
    loader.build(context);
    controller.unBinding({
      RequestKey.cardPan: cardPan,
      RequestKey.phone: phone,
    }, (data) {
      Navigator.of(context).pop();
      onSuccess();
    }, (message) {
      Navigator.of(context).pop();
      checkConnection((status) {
        if (status != "Connect")
          error(context, "Tidak terhubung ke internet.");
        else
          error(context, message);
      });
    });
  }

  // Do UnBinding Link Aja
  void doUnBindingLinkAja(var bindingID, VoidCallback onSuccess) {
    loader.build(context);
    controller.unBindingLinkAja(bindingID, (data) {
      Navigator.of(context).pop();
      success(context, data);
      onSuccess();
    }, (message) {
      Navigator.of(context).pop();
      checkConnection((status) {
        if (status != "Connect")
          error(context, "Tidak terhubung ke internet.");
        else
          error(context, message);
      });
    });
  }
}

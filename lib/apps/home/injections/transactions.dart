import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:jmtopay/api/transactions/transaction.dart';
import 'package:jmtopay/helper/global.dart';
import 'package:jmtopay/helper/request.dart';
import 'package:jmtopay/helper/sheet.dart';
import 'package:jmtopay/helper/url.dart';
import 'package:jmtopay/utils/component.dart';

class TransactionsInjections {
  Dio dio;
  RequestModel requestModel;
  TransactionController controller;
  BuildContext context;

  // Setup Initialize
  void initialize(BuildContext context) {
    this.context = context;
    this.dio = models.initial(BaseURLAPI.uri);
    this.requestModel = RequestModel(dio: dio);

    this.controller = TransactionController(
        context: context, requestModel: this.requestModel);
  }

  void loadTransaction(Function(List<dynamic>) onLoadedItem) {
    controller.lists((data) {
      List<dynamic> items = [];
      data.forEach((item) {
        items.add(item);
      });
      onLoadedItem(items);
    }, (message) {
      checkConnection((status) {
        if (status != "Connect")
          error(context, "Tidak terhubung ke internet.");
        else
          error(context, message);
      });
    });
  }

  void loadDetailTransaction(
      var transactionID, Function(dynamic) onLoadedItem) {
    controller.detail(transactionID, (data) {
      onLoadedItem(data);
    }, (message) {
      checkConnection((status) {
        if (status != "Connect")
          error(context, "Tidak terhubung ke internet.");
        else
          error(context, message);
      });
    });
  }

  // Do Order
  void doOrder(
      var productID, var bindingID, String note, VoidCallback onSuccess) {
    loader.build(context);
    controller.order({
      RequestKey.productID: productID,
      RequestKey.bindingID: bindingID,
      RequestKey.desc: (note.isNotEmpty) ? note : "-"
    }, (data) {
      Navigator.of(context).pop();
      onSuccess();
    }, (message) {
      Navigator.of(context).pop();
      checkConnection((status) {
        if (status != "Connect")
          error(context, "Tidak terhubung ke internet.");
        else
          error(context, message);
      });
    });
  }

  // Do Order Midtrans
  void doOrderMidtrans(
      var productID, String note, Function(dynamic) onSuccess) {
    loader.build(context);
    controller.orderMidtrans({
      RequestKey.productID: productID,
      RequestKey.desc: (note.isNotEmpty) ? note : "-"
    }, (data) {
      Navigator.of(context).pop();
      onSuccess(data);
    }, (message) {
      Navigator.of(context).pop();
      checkConnection((status) {
        if (status != "Connect")
          error(context, "Tidak terhubung ke internet.");
        else
          error(context, message);
      });
    });
  }

  // Do Order Link Aja
  void doOrderLinkAja(
      var productID, var bindingID, String note, Function(String) onSuccess) {
    loader.build(context);
    controller.orderLinkAja({
      RequestKey.productID: productID,
      RequestKey.desc: (note.isNotEmpty) ? note : "-",
      RequestKey.bindingID: bindingID
    }, (data) {
      Navigator.of(context).pop();
      onSuccess(data);
    }, (message) {
      Navigator.of(context).pop();
      checkConnection((status) {
        if (status != "Connect")
          error(context, "Tidak terhubung ke internet.");
        else
          error(context, message);
      });
    });
  }
}

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:jmtopay/api/cards/linkaja.dart';
import 'package:jmtopay/helper/global.dart';
import 'package:jmtopay/helper/request.dart';
import 'package:jmtopay/helper/sheet.dart';
import 'package:jmtopay/utils/component.dart';

class LinkAjaInjections {
  Dio dio;
  RequestModel requestModel;
  LinkAjaController controller;
  BuildContext context;

  // Setup Initialize
  void initialize(BuildContext context) {
    this.context = context;
    this.dio = models.initial("");
    this.requestModel = RequestModel(dio: dio);

    this.controller =
        LinkAjaController(context: context, requestModel: this.requestModel);
  }

  // Do Binding Link Aja
  void doBindingLinkAja(String token, Function(dynamic) onSuccess) {
    loader.build(context);
    controller.openFrame({"Message": token}, (data) {
      Navigator.of(context).pop();
      onSuccess(data);
    }, (message) {
      Navigator.of(context).pop();
      checkConnection((status) {
        if (status != "Connect")
          error(context, "Tidak terhubung ke internet.");
        else
          error(context, message);
      });
    });
  }
}

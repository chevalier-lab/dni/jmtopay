import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:jmtopay/api/payments/payment.dart';
import 'package:jmtopay/helper/global.dart';
import 'package:jmtopay/helper/request.dart';
import 'package:jmtopay/helper/sheet.dart';
import 'package:jmtopay/helper/url.dart';

class PaymentsInjections {
  Dio dio;
  RequestModel requestModel;
  PaymentController controller;
  BuildContext context;

  // Setup Initialize
  void initialize(BuildContext context) {
    this.context = context;
    this.dio = models.initial(BaseURLAPI.uri);
    this.requestModel = RequestModel(dio: dio);

    this.controller =
        PaymentController(context: context, requestModel: this.requestModel);
  }

  void loadPayments(Function(List<dynamic>) onLoadedItem) {
    controller.lists((data) {
      onLoadedItem(data);
    }, (message) {
      checkConnection((status) {
        if (status != "Connect")
          error(context, "Tidak terhubung ke internet.");
        else
          error(context, message);
      });
    });
  }
}

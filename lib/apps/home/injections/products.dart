import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:jmtopay/api/products/product.dart';
import 'package:jmtopay/helper/global.dart';
import 'package:jmtopay/helper/request.dart';
import 'package:jmtopay/helper/sheet.dart';
import 'package:jmtopay/helper/url.dart';

class ProductsInjections {
  Dio dio;
  RequestModel requestModel;
  ProductController controller;
  BuildContext context;

  // Setup Initialize
  void initialize(BuildContext context) {
    this.context = context;
    this.dio = models.initial(BaseURLAPI.uri);
    this.requestModel = RequestModel(dio: dio);

    this.controller =
        ProductController(context: context, requestModel: this.requestModel);
  }

  void loadProducts(Function(List<dynamic>) onLoadedItem) {
    controller.lists((data) {
      List<dynamic> items = [];
      data.forEach((item) {
        items.add(item);
      });
      onLoadedItem(items);
    }, (message) {
      checkConnection((status) {
        if (status != "Connect")
          error(context, "Tidak terhubung ke internet.");
        else
          error(context, message);
      });
    });
  }
}

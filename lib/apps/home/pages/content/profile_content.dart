import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/auth/provider.dart';
import 'package:jmtopay/apps/home/components/profile_menu.dart';
import 'package:jmtopay/apps/home/components/profilebar.dart';
import 'package:jmtopay/apps/home/items/item_profile_menu.dart';
import 'package:jmtopay/helper/cache.dart';
import 'package:jmtopay/helper/url.dart';
import 'package:jmtopay/models/home/wallet_md.dart';
import 'package:jmtopay/models/home_md.dart';

class ProfileContent extends StatelessWidget {
  final WalletMD walletProvider;
  final HomeMD provider;

  ProfileContent({@required this.walletProvider, @required this.provider});

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
        child: FutureBuilder(
          future: getAuth(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ProfileBar(profile: {
                RequestKey.cmName: snapshot.data[RequestKey.cmName],
                RequestKey.email: snapshot.data[RequestKey.email]
              });
            }
            return CircularProgressIndicator();
          },
        ),
        margin: EdgeInsets.symmetric(horizontal: 16),
      ),
      SizedBox(height: 8),
      Container(
        width: MediaQuery.of(context).size.width,
        child: ProfileMenu(
            items: this.walletProvider.profileMenus,
            onItemClick: (dynamic item) {}),
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      ),
      Container(
        child: ItemProfileMenu(
            item: this.walletProvider.profileMenus[3], onClick: () {}),
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16), color: Colors.white),
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      ),
      Container(
        child: ItemProfileMenu(
            item: this.walletProvider.profileMenus[4], onClick: () {}),
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16), color: Colors.white),
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      ),
      Container(
        child: ItemProfileMenu(
            item: this.walletProvider.profileMenus[5],
            onClick: () {
              CacheHelper.remove();
              Navigator.of(context).pushNamedAndRemoveUntil(
                  AuthProvider.routeName, (route) => false);
            }),
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16), color: Colors.white),
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      ),
    ]);
  }
}

import 'package:flutter/cupertino.dart';
import 'package:jmtopay/apps/home/components/services.dart';
import 'package:jmtopay/apps/home/pages/content/builder/card_builder.dart';
import 'package:jmtopay/apps/home/pages/content/builder/product_builder.dart';
import 'package:jmtopay/apps/home/pages/content/builder/transaction_builder.dart';
import 'package:jmtopay/models/home/wallet_md.dart';
import 'package:jmtopay/models/home_md.dart';

class WalletContent extends StatelessWidget {
  final WalletMD walletProvider;
  final HomeMD provider;

  WalletContent({@required this.walletProvider, @required this.provider});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CardBuilder(walletProvider: walletProvider, provider: provider),
        SizedBox(height: 16),
        Services(
          items: walletProvider.services,
          isCollapse: false,
          onItemClick: (item) {},
          onCollapse: () {},
        ),
        SizedBox(height: 16),
        ProductBuilder(walletProvider: walletProvider, provider: provider),
        SizedBox(height: 16),
        TransactionBuilder(walletProvider: walletProvider, provider: provider),
        SizedBox(height: 16),
      ],
    );
  }
}

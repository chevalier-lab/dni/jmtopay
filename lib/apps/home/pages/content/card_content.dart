import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/home/components/transaction_history.dart';
import 'package:jmtopay/apps/home/pages/content/builder/cards_builder.dart';
import 'package:jmtopay/models/home/wallet_md.dart';
import 'package:jmtopay/models/home_md.dart';

class CardContent extends StatelessWidget {
  final WalletMD walletProvider;
  final HomeMD provider;

  CardContent({@required this.walletProvider, @required this.provider});

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      CardsBuilder(walletProvider: walletProvider, provider: provider),
      TransactionHistory(
          items: walletProvider.historyTransactions,
          isLimited: true,
          onItemClick: (item) {},
          onLoadMore: () {}),
      SizedBox(height: 16),
    ]);
  }
}

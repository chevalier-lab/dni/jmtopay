import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/home/components/notification_list.dart';
import 'package:jmtopay/helper/str.dart';
import 'package:jmtopay/models/home/wallet_md.dart';
import 'package:jmtopay/models/home_md.dart';

class NotificationContent extends StatelessWidget {
  final WalletMD walletProvider;
  final HomeMD provider;

  NotificationContent({@required this.walletProvider, @required this.provider});

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      NotificationList(
          title: STR.newNotification,
          items: walletProvider.notifications,
          onItemClick: (dynamic item) {},
          onLoadMore: () {})
    ]);
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/home/components/products.dart';
import 'package:jmtopay/apps/home/injections/products.dart';
import 'package:jmtopay/apps/home/injections/transactions.dart';
import 'package:jmtopay/apps/home/popups/detail_product.dart';
import 'package:jmtopay/helper/sheet.dart';
import 'package:jmtopay/helper/url.dart';
import 'package:jmtopay/models/home/wallet_md.dart';
import 'package:jmtopay/models/home_md.dart';

class ProductBuilder extends StatefulWidget {
  final WalletMD walletProvider;
  final HomeMD provider;

  ProductBuilder({@required this.walletProvider, @required this.provider});

  @override
  State<StatefulWidget> createState() => _ProductBuilder();
}

class _ProductBuilder extends State<ProductBuilder> {
  var injections = ProductsInjections();
  var transactionInjections = TransactionsInjections();

  @override
  void initState() {
    super.initState();

    injections.initialize(context);
    transactionInjections.initialize(context);

    setupProductItems();
  }

  void setupProductItems() {
    injections
        .loadProducts((items) => widget.walletProvider.setProducts(items));
  }

  void setupTransactionItems() {
    transactionInjections.loadTransaction(
        (items) => widget.walletProvider.setTransactionHistory(items));
  }

  // Open Detail Product
  Future openDetailProduct(BuildContext context, dynamic product) async {
    var response =
        await Navigator.of(context).push(new MaterialPageRoute<dynamic>(
            builder: (BuildContext context) {
              return new DetailProductPopups(
                  injections: injections, product: product);
            },
            fullscreenDialog: true));
    if (response != null) {
      if (response["status"]) {
        success(context, response[RequestKey.message]);
        setupTransactionItems();
      } else
        error(context, response[RequestKey.message]);
    }
    setupProductItems();
  }

  @override
  Widget build(BuildContext context) {
    return Products(
        items: widget.walletProvider.products,
        isLimited: false,
        onItemClick: (item) {
          openDetailProduct(context, item);
        },
        onLoadMore: () {});
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/home/components/card_mine.dart';
import 'package:jmtopay/apps/home/injections/cards.dart';
import 'package:jmtopay/apps/home/pages/new_card.dart';
import 'package:jmtopay/helper/img.dart';
import 'package:jmtopay/helper/sheet.dart';
import 'package:jmtopay/helper/tint.dart';
import 'package:jmtopay/helper/url.dart';
import 'package:jmtopay/models/home/wallet_md.dart';
import 'package:jmtopay/models/home_md.dart';

class CardsBuilder extends StatefulWidget {
  final WalletMD walletProvider;
  final HomeMD provider;

  CardsBuilder({@required this.walletProvider, @required this.provider});

  @override
  State<StatefulWidget> createState() => _CardsBuilder();
}

class _CardsBuilder extends State<CardsBuilder> {
  final injection = CardInjections();
  bool isAlreadyBinding = false;
  bool isLoadCheckBinding = true;
  int isAlreadySetChange = 0;
  List<dynamic> cards = [], cardsOri = [];

  @override
  void initState() {
    super.initState();

    injection.initialize(context);
    setupCheckBindingData();
  }

  // Check Binding Data
  void setupCheckBindingData() {
    injection.checkBinding((List data) {
      if (data.length > 0) {
        List<dynamic> temp = [];
        List<dynamic> tempOri = [];
        data.forEach((element) {
          if (element[RequestKey.bindingStatus] == "DONE") {
            tempOri.add(element);
            if (element[RequestKey.cardPan].toString().length == 16) {
              temp.add({
                "icon": IMG.bri,
                "full_name": element[RequestKey.cmName],
                "valid_date": element[RequestKey.expDate],
                "card_number": [
                  element[RequestKey.cardPan].toString().substring(0, 3),
                  element[RequestKey.cardPan].toString().substring(4, 7),
                  element[RequestKey.cardPan].toString().substring(8, 11),
                  element[RequestKey.cardPan].toString().substring(12, 15)
                ],
                "balance": 0,
                "binding_id": element[RequestKey.bindingID]
              });
            } else {
              temp.add({
                "icon": IMG.linkaja,
                "full_name": element[RequestKey.cmName],
                "valid_date": element[RequestKey.expDate],
                "card_number": [
                  element[RequestKey.phoneNumber].toString().substring(0, 3),
                  element[RequestKey.phoneNumber].toString().substring(4, 7),
                  element[RequestKey.phoneNumber].toString().substring(8)
                ],
                "balance": 0,
                "binding_id": element[RequestKey.bindingID]
              });
            }
          }
        });

        setState(() {
          cards = temp;
          cardsOri = tempOri;
          isLoadCheckBinding = false;
          isAlreadyBinding = true;
        });
      } else {
        setState(() {
          cards = [];
          cardsOri = [];
          isLoadCheckBinding = false;
          isAlreadyBinding = false;
        });
      }
    });
  }

  void doUnBinding(dynamic item) {
    injection.doUnBinding(item[RequestKey.cardPan], item[RequestKey.phone], () {
      success(context, "Berhasil melakukan unbinding");
      setupCheckBindingData();
    });
  }

  void doUnBindingLinkAja(var bindingID) {
    injection.doUnBindingLinkAja(bindingID, () => setupCheckBindingData());
  }

  @override
  Widget build(BuildContext context) {
    return (!isLoadCheckBinding)
        ? (isAlreadyBinding && cards.length > 0)
            ? CardMine(
                items: cards,
                onItemClick: (dynamic item) {},
                onDeleteItem: (dynamic item) {
                  if (item["icon"] == IMG.bri) {
                    var itemOri;
                    cardsOri.forEach((element) {
                      if (element[RequestKey.bindingID] == item["binding_id"])
                        itemOri = element;
                    });

                    if (itemOri != null) doUnBinding(itemOri);
                  } else
                    doUnBindingLinkAja(item["binding_id"]);
                },
              )
            : GestureDetector(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 12, vertical: 16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Tint.danger,
                  ),
                  child: Text(
                      "No card binding done yet, click to binding new card",
                      style: TextStyle(color: Tint.white)),
                ),
                onTap: () {
                  widget.provider.setCurrentPage(NewCardPageProvider.pageName);
                },
              )
        : Center(child: CircularProgressIndicator());
  }
}

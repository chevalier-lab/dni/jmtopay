import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/home/components/card_full.dart';
import 'package:jmtopay/apps/home/injections/cards.dart';
import 'package:jmtopay/apps/home/pages/link_card.dart';
import 'package:jmtopay/helper/img.dart';
import 'package:jmtopay/helper/sheet.dart';
import 'package:jmtopay/helper/tint.dart';
import 'package:jmtopay/helper/url.dart';
import 'package:jmtopay/models/home/wallet_md.dart';
import 'package:jmtopay/models/home_md.dart';

class CardBuilder extends StatefulWidget {
  final WalletMD walletProvider;
  final HomeMD provider;

  CardBuilder({@required this.walletProvider, @required this.provider});

  @override
  State<StatefulWidget> createState() => _CardBuilder();
}

class _CardBuilder extends State<CardBuilder> {
  final injection = CardInjections();
  bool isAlreadyBinding = false;
  bool isLoadCheckBinding = true;
  int isAlreadySetChange = 0;
  dynamic card, oriCard;

  @override
  void initState() {
    super.initState();

    injection.initialize(context);
    setupCheckBindingData();
  }

  // Check Binding Data
  void setupCheckBindingData() {
    injection.checkBinding((List data) {
      if (data.length > 0) {
        List<dynamic> temp = [];
        List<dynamic> tempOri = [];
        data.forEach((element) {
          if (element[RequestKey.bindingStatus] == "DONE") {
            if (element[RequestKey.cardPan].toString().length == 16) {
              temp.add({
                "icon": IMG.bri,
                "full_name": element[RequestKey.cmName],
                "valid_date": element[RequestKey.expDate],
                "card_number": [
                  element[RequestKey.cardPan].toString().substring(0, 3),
                  element[RequestKey.cardPan].toString().substring(4, 7),
                  element[RequestKey.cardPan].toString().substring(8, 11),
                  element[RequestKey.cardPan].toString().substring(12, 15)
                ],
                "balance": 0
              });
              tempOri.add(element);
            } else {
              temp.add({
                "icon": IMG.linkaja,
                "full_name": element[RequestKey.cmName],
                "valid_date": element[RequestKey.expDate],
                "card_number": [
                  element[RequestKey.phoneNumber].toString().substring(0, 3),
                  element[RequestKey.phoneNumber].toString().substring(4, 7),
                  element[RequestKey.phoneNumber].toString().substring(8)
                ],
                "balance": 0
              });
              tempOri.add(element);
            }
          }
        });

        setState(() {
          if (temp.length > 0) {
            card = temp[0];
            oriCard = tempOri[0];
          }
          isLoadCheckBinding = false;
          isAlreadyBinding = true;
        });
      } else {
        setState(() {
          isLoadCheckBinding = false;
          isAlreadyBinding = false;
        });
      }
    });
  }

  void doUnBinding() {
    injection.doUnBinding(
        oriCard[RequestKey.cardPan], oriCard[RequestKey.phone], () {
      success(context, "Berhasil melakukan unbinding");
      setupCheckBindingData();
    });
  }

  void doUnBindingLinkAja() {
    injection.doUnBindingLinkAja(
        oriCard[RequestKey.bindingID], () => setupCheckBindingData());
  }

  @override
  Widget build(BuildContext context) {
    return (!isLoadCheckBinding)
        ? (isAlreadyBinding && card != null)
            ? CardFull(
                icon: card["icon"],
                fullName: card["full_name"],
                validDate: card["valid_date"],
                cardNumber: card["card_number"],
                onUnBinding: () {
                  if (card["icon"] == IMG.bri) {
                    doUnBinding();
                  } else
                    doUnBindingLinkAja();
                },
              )
            : GestureDetector(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 12, vertical: 16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Tint.danger,
                  ),
                  child: Text(
                      "No card binding done yet, click to binding new card",
                      style: TextStyle(color: Tint.white)),
                ),
                onTap: () {
                  widget.provider.setCurrentPage(LinkCardPageProvider.pageName);
                },
              )
        : Center(child: CircularProgressIndicator());
  }
}

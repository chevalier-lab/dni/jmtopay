import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/home/components/transaction_history.dart';
import 'package:jmtopay/apps/home/injections/transactions.dart';
import 'package:jmtopay/apps/home/popups/detail_transaction.dart';
import 'package:jmtopay/helper/sheet.dart';
import 'package:jmtopay/helper/url.dart';
import 'package:jmtopay/models/home/wallet_md.dart';
import 'package:jmtopay/models/home_md.dart';

class TransactionBuilder extends StatefulWidget {
  final WalletMD walletProvider;
  final HomeMD provider;

  TransactionBuilder({@required this.walletProvider, @required this.provider});

  @override
  State<StatefulWidget> createState() => _TransactionBuilder();
}

class _TransactionBuilder extends State<TransactionBuilder> {
  var injections = TransactionsInjections();

  @override
  void initState() {
    super.initState();

    injections.initialize(context);

    setupTransactionItems();
  }

  void setupTransactionItems() {
    injections.loadTransaction(
        (items) => widget.walletProvider.setTransactionHistory(items));
  }

  // Open Detail Transaction
  Future openDetailTransaction(
      BuildContext context, dynamic transaction) async {
    var response =
        await Navigator.of(context).push(new MaterialPageRoute<dynamic>(
            builder: (BuildContext context) {
              return new DetailTransactionPopups(
                  injections: injections, transaction: transaction);
            },
            fullscreenDialog: true));
    if (response != null) {
      if (response["status"]) {
        success(context, response[RequestKey.message]);
        setupTransactionItems();
      } else
        error(context, response[RequestKey.message]);
    }
  }

  @override
  Widget build(BuildContext context) {
    return TransactionHistory(
        items: widget.walletProvider.historyTransactions,
        isLimited: false,
        onItemClick: (item) {
          openDetailTransaction(context, item);
        },
        onLoadMore: () {});
  }
}

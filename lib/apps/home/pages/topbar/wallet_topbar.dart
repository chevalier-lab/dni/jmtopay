import 'package:flutter/cupertino.dart';
import 'package:jmtopay/apps/home/components/searchbar.dart';

class WalletTopBar extends StatelessWidget {
  final Function(String) onSearch;

  WalletTopBar({@required this.onSearch});

  @override
  Widget build(BuildContext context) {
    return SearchBar(onSearch: (String search) => {print(search)});
  }
}

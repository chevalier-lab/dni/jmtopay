import 'package:flutter/cupertino.dart';
import 'package:jmtopay/apps/home/components/topbar.dart';

class NotificationTopBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: TopBar(
        title: "Notification",
        actions: [],
        isCanBeBack: true,
        onBackPress: () {},
      ),
      margin: EdgeInsets.only(bottom: 16),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/home/components/topbar.dart';
import 'package:jmtopay/apps/home/pages/link_card.dart';
import 'package:jmtopay/helper/tint.dart';
import 'package:jmtopay/models/home_md.dart';

class CardTopBar extends StatelessWidget {
  final HomeMD provider;

  CardTopBar({@required this.provider});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TopBar(
        title: "Kartu Debit Anda",
        actions: [
          GestureDetector(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 8),
              child: Icon(
                Icons.add,
                color: Tint.field,
              ),
            ),
            onTap: () {
              provider.setCurrentPage(LinkCardPageProvider.pageName);
            },
          )
        ],
        isCanBeBack: true,
        onBackPress: () {},
      ),
      margin: EdgeInsets.only(bottom: 16),
    );
  }
}

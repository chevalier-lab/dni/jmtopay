import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/home/items/item_bottom_navigation.dart';
import 'package:jmtopay/apps/home/pages/content/card_content.dart';
import 'package:jmtopay/apps/home/pages/content/notification_content.dart';
import 'package:jmtopay/apps/home/pages/content/profile_content.dart';
import 'package:jmtopay/apps/home/pages/content/wallet_content.dart';
import 'package:jmtopay/apps/home/pages/topbar/card_topbar.dart';
import 'package:jmtopay/apps/home/pages/topbar/notification_topbar.dart';
import 'package:jmtopay/apps/home/pages/topbar/profile_topbar.dart';
import 'package:jmtopay/apps/home/pages/topbar/wallet_topbar.dart';
import 'package:jmtopay/helper/tint.dart';
import 'package:jmtopay/models/home/wallet_md.dart';
import 'package:jmtopay/models/home_md.dart';
import 'package:provider/provider.dart';

class WalletPageProvider {
  static const String pageName = "/wallet";

  static Widget init(BuildContext context) {
    return ChangeNotifierProvider<WalletMD>(
        create: (_) => WalletMD(), child: WalletPage());
  }
}

class WalletPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _WalletPage();
}

class _WalletPage extends State<WalletPage> {
  @override
  void initState() {
    super.initState();
  }

  Widget setupTopBar(HomeMD provider) {
    StatelessWidget selectedTopBar = WalletTopBar(onSearch: (String search) {});

    int selectedPosition = 0;
    int index = 0;
    provider.bottomNavigations.forEach((element) {
      if (element["is_active"])
        selectedPosition = index;
      else
        index++;
    });

    switch (selectedPosition) {
      case 1:
        selectedTopBar = NotificationTopBar();
        break;
      case 3:
        selectedTopBar = CardTopBar(provider: provider);
        break;
      case 4:
        selectedTopBar = ProfileTopBar();
        break;
    }

    return selectedTopBar;
  }

  Widget setupPage(WalletMD walletProvider, HomeMD provider) {
    StatelessWidget selectedPage;

    int selectedPosition = 0;
    int index = 0;
    provider.bottomNavigations.forEach((element) {
      if (element["is_active"])
        selectedPosition = index;
      else
        index++;
    });

    switch (selectedPosition) {
      case 1:
        selectedPage = NotificationContent(
            walletProvider: walletProvider, provider: provider);
        break;
      case 3:
        selectedPage =
            CardContent(walletProvider: walletProvider, provider: provider);
        break;
      case 4:
        selectedPage =
            ProfileContent(walletProvider: walletProvider, provider: provider);
        break;
      default:
        selectedPage =
            WalletContent(walletProvider: walletProvider, provider: provider);
        break;
    }

    return Expanded(
        child: SingleChildScrollView(
      child: selectedPage,
    ));
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<HomeMD>(context);
    final walletProvider = Provider.of<WalletMD>(context);

    return Scaffold(
      backgroundColor: Tint.light,
      body: SafeArea(
          child: Column(
        children: [
          this.setupTopBar(provider),
          this.setupPage(walletProvider, provider)
        ],
      )),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: 2,
          backgroundColor: Colors.white,
          showSelectedLabels: false,
          fixedColor: Colors.white,
          type: BottomNavigationBarType.fixed,
          iconSize: 24,
          showUnselectedLabels: false,
          items: List.generate(provider.bottomNavigations.length, (index) {
            dynamic item = provider.bottomNavigations[index];
            return itemButtonNavigation(item, () {
              if (index != 2) provider.setSelectedBottomNavigation(index);
            });
          })),
    );
  }
}

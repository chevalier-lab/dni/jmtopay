import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/home/components/card_full.dart';
import 'package:jmtopay/apps/home/components/topbar.dart';
import 'package:jmtopay/apps/home/injections/cards.dart';
import 'package:jmtopay/apps/home/injections/linkaja.dart';
import 'package:jmtopay/apps/home/pages/link_card.dart';
import 'package:jmtopay/apps/home/pages/wallet.dart';
import 'package:jmtopay/apps/home/popups/binding_link_aja.dart';
import 'package:jmtopay/apps/home/popups/otp.dart';
import 'package:jmtopay/components/custom_field.dart';
import 'package:jmtopay/helper/img.dart';
import 'package:jmtopay/helper/sheet.dart';
import 'package:jmtopay/helper/tint.dart';
import 'package:jmtopay/models/home/new_card_md.dart';
import 'package:jmtopay/models/home_md.dart';
import 'package:jmtopay/utils/device.dart';
import 'package:jmtopay/utils/location.dart';
import 'package:provider/provider.dart';

class NewCardPageProvider {
  static const String pageName = "/new-card";

  static Widget init(BuildContext context) {
    return ChangeNotifierProvider<NewCardMD>(
        create: (_) => NewCardMD(), child: NewCardPage());
  }
}

class NewCardPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _NewCardPage();
}

class _NewCardPage extends State<NewCardPage> {
  final injections = CardInjections();
  final linkAjaInjection = LinkAjaInjections();
  dynamic latLng = {"lat": "0.0000000", "lng": "0.0000000"};
  String deviceID = "";
  final location = Location();
  final device = Device();

  @override
  void initState() {
    super.initState();

    setupDeviceService();
    setupLocationServices();

    linkAjaInjection.initialize(context);

    injections.initialize(context);
  }

  // Setup Device Service
  void setupDeviceService() async {
    String tempDeviceID = await device.getID();
    setState(() {
      deviceID = tempDeviceID;
    });
    print("DEVICE_ID $deviceID");
  }

  // Setup Location Services
  void setupLocationServices() async {
    location.getCurrentLocation((data) {
      setState(() {
        this.latLng = data;
      });
      print("LATLONG $data");
    });
  }

  // Do Binding Card
  void doBindingCard(HomeMD provider, String cardPan, String expDate) {
    if (provider.currentLocal["url"] == IMG.bri)
      injections.doBinding(
          cardPan,
          deviceID,
          this.latLng["lat"],
          this.latLng["lng"],
          expDate.split("/").join(""),
          () => openOTPPopups(context, provider, cardPan));
    else
      injections.doBindingLinkAja(
          cardPan,
          deviceID,
          this.latLng["lat"],
          this.latLng["lng"],
          (data) => openLinkAjaPopups(context, provider, data));
  }

  void checkLinkAja(dynamic data) {
    linkAjaInjection.doBindingLinkAja(data["pgpToken"], (res) => null);
  }

  // Do Binding Verify
  void doBindingVerify(HomeMD provider, String pin, String card) {
    injections.doVerify(
        pin, card, () => provider.setCurrentPage(WalletPageProvider.pageName));
    // provider.setCurrentPage(WalletPageProvider.pageName);
  }

  // Open Detail Product
  Future openLinkAjaPopups(
      BuildContext context, HomeMD provider, dynamic data) async {
    await Navigator.of(context).push(new MaterialPageRoute<dynamic>(
        builder: (BuildContext context) {
          return BindingLinkAja(
            redirectURL: data["redirectUrl"],
            token: data["pgpToken"],
          );
        },
        fullscreenDialog: true));
    provider.setCurrentPage(WalletPageProvider.pageName);
  }

  // Open Detail Product
  Future openOTPPopups(
      BuildContext context, HomeMD provider, String card) async {
    dynamic response =
        await Navigator.of(context).push(new MaterialPageRoute<dynamic>(
            builder: (BuildContext context) {
              return OTPPage(provider: provider);
            },
            fullscreenDialog: true));
    if (response != null) {
      if (response["status"]) {
        doBindingVerify(provider, response["pin"], card);
        // success(context, response["message"]);
      } else
        error(context, response["message"]);
    }
  }

  Widget setupTopBar(HomeMD provider) => TopBar(
      title: "New Card",
      actions: [],
      isCanBeBack: true,
      onBackPress: () =>
          provider.setCurrentPage(LinkCardPageProvider.pageName));

  Widget setupPageCard(NewCardMD newCardProvider, HomeMD provider) {
    return Column(
      children: [
        Container(
            child: CardFull(
              icon: newCardProvider.newCard["icon"],
              fullName: newCardProvider.newCard["full_name"],
              validDate: newCardProvider.newCard["valid_date"],
              cardNumber: newCardProvider.newCard["card_number"],
              onUnBinding: () {
                newCardProvider.checkSetCardNumber("");
              },
            ),
            margin: EdgeInsets.only(top: 16, bottom: 8)),
        Container(
            child: FieldControl(
              field: newCardProvider.fieldNewCard[0],
              isAvalid: (newCardProvider.checkCardNumber()),
              onTyping: (String value) =>
                  newCardProvider.checkSetCardNumber(value),
            ),
            margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8)),
        Row(
          children: [
            SizedBox(width: 8),
            Expanded(
                child: Container(
                    child: FieldControl(
                      field: newCardProvider.fieldNewCard[2],
                      isAvalid: (newCardProvider.checkMonth()),
                      onTyping: (String _) => newCardProvider.setValidDate(),
                    ),
                    margin: EdgeInsets.symmetric(horizontal: 8, vertical: 8))),
            Expanded(
                child: Container(
                    child: FieldControl(
                      field: newCardProvider.fieldNewCard[3],
                      isAvalid: (newCardProvider.checkYear()),
                      onTyping: (String _) => newCardProvider.setValidDate(),
                    ),
                    margin: EdgeInsets.symmetric(horizontal: 8, vertical: 8))),
            SizedBox(width: 8),
          ],
        ),
      ],
    );
  }

  Widget setupPagePhone(NewCardMD newCardProvider, HomeMD provider) {
    return Column(
      children: [
        Container(
            child: CardFull(
              icon: provider.currentLocal["url"],
              fullName: newCardProvider.newCard["full_name"],
              validDate: newCardProvider.newCard["valid_date"],
              cardNumber: newCardProvider.newCard["card_number"],
              onUnBinding: () {
                newCardProvider.checkSetPhoneNumber("");
              },
            ),
            margin: EdgeInsets.only(top: 16, bottom: 8)),
        Container(
            child: FieldControl(
              field: newCardProvider.fieldPhoneNumber[0],
              isAvalid: (newCardProvider.checkPhoneNumber()),
              onTyping: (String value) =>
                  newCardProvider.checkSetPhoneNumber(value),
            ),
            margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8)),
      ],
    );
  }

  Widget setupPage(NewCardMD newCardProvider, HomeMD provider) {
    return Expanded(
        child: SingleChildScrollView(
      child: provider.currentLocal["url"] == IMG.bri
          ? setupPageCard(newCardProvider, provider)
          : setupPagePhone(newCardProvider, provider),
    ));
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<HomeMD>(context);
    final newCardProvider = Provider.of<NewCardMD>(context);

    return Scaffold(
      backgroundColor: Tint.white,
      body: SafeArea(
          child: Column(
        children: [
          this.setupTopBar(provider),
          this.setupPage(newCardProvider, provider)
        ],
      )),
      bottomNavigationBar: Container(
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: (newCardProvider.isValid) ? Tint.main : Tint.light,
              textStyle: const TextStyle(fontSize: 16),
              padding: EdgeInsets.symmetric(vertical: 16)),
          onPressed: () => {
            if (newCardProvider.isValid)
              doBindingCard(
                  provider,
                  newCardProvider.newCard["card_number"].join(""),
                  newCardProvider.newCard["valid_date"])
            else
              null
          },
          child: Text(
            "Next",
            style: TextStyle(
                color: (newCardProvider.isValid) ? Tint.white : Colors.black),
          ),
        ),
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 16),
        color: Colors.white,
      ),
    );
  }
}

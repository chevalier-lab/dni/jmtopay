import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/home/components/banks.dart';
import 'package:jmtopay/apps/home/components/topbar.dart';
import 'package:jmtopay/apps/home/pages/new_card.dart';
import 'package:jmtopay/apps/home/pages/wallet.dart';
import 'package:jmtopay/helper/str.dart';
import 'package:jmtopay/helper/tint.dart';
import 'package:jmtopay/models/home/link_card_md.dart';
import 'package:jmtopay/models/home_md.dart';
import 'package:provider/provider.dart';

class LinkCardPageProvider {
  static const String pageName = "/link-card";

  static Widget init(BuildContext context) {
    return ChangeNotifierProvider<LinkCardMD>(
        create: (_) => LinkCardMD(), child: LinkCardPage());
  }
}

class LinkCardPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LinkCardPage();
}

class _LinkCardPage extends State<LinkCardPage> {
  Widget setupTopBar(HomeMD provider) => TopBar(
      title: "Link Card",
      actions: [
        GestureDetector(
          child: Text(
            "Skip",
            style: TextStyle(
              color: Tint.text,
              fontSize: 14,
            ),
          ),
          onTap: () => provider.setCurrentPage(WalletPageProvider.pageName),
        )
      ],
      isCanBeBack: true,
      onBackPress: () => provider.setCurrentPage(WalletPageProvider.pageName));

  Widget setupPage(LinkCardMD linkCardProvider, HomeMD provider) {
    return Expanded(
        child: SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: 16),
          Container(
            child: Banks(
              title: STR.bankAccount,
              items: linkCardProvider.locals,
              onItemClick: (item) {
                provider.setCurrentLocal(item);
                provider.setCurrentPage(NewCardPageProvider.pageName);
              },
              onCollapse: () {},
            ),
            margin: EdgeInsets.symmetric(horizontal: 16),
          ),
          SizedBox(height: 16),
          Container(
            child: Banks(
              title: STR.internationCard,
              items: linkCardProvider.globals,
              onItemClick: (item) => {},
              onCollapse: () {},
            ),
            margin: EdgeInsets.symmetric(horizontal: 16),
          )
        ],
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<HomeMD>(context);
    final linkCardProvider = Provider.of<LinkCardMD>(context);

    return Scaffold(
      backgroundColor: Tint.light,
      body: SafeArea(
          child: Column(
        children: [
          this.setupTopBar(provider),
          this.setupPage(linkCardProvider, provider)
        ],
      )),
    );
  }
}

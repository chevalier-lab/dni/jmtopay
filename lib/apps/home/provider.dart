import 'package:flutter/cupertino.dart';
import 'package:jmtopay/apps/home/view.dart';
import 'package:jmtopay/models/home_md.dart';
import 'package:provider/provider.dart';

class HomeProvider {
  static final String routeName = "/home";

  static Widget init() => ChangeNotifierProvider<HomeMD>(
        create: (context) => HomeMD(),
        child: HomeView(),
      );
}

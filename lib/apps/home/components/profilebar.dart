import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/components/custom_image.dart';
import 'package:jmtopay/helper/img.dart';
import 'package:jmtopay/helper/tint.dart';
import 'package:jmtopay/helper/url.dart';

class ProfileBar extends StatelessWidget {
  final dynamic profile;

  ProfileBar({@required this.profile});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          ImageSet(width: 48, height: 48, url: IMG.user),
          SizedBox(width: 8),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(this.profile[RequestKey.cmName],
                  style: TextStyle(color: Tint.heading, fontSize: 14)),
              Text(this.profile[RequestKey.email],
                  style: TextStyle(color: Tint.text, fontSize: 12)),
            ],
          )),
          Container(
            child: Text(
              "Verified",
              style: TextStyle(color: Tint.success, fontSize: 12),
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: Tint.warningPale),
          ),
          SizedBox(width: 8),
          // Icon(
          //   Icons.chevron_right_outlined,
          //   color: Tint.field,
          // )
        ],
      ),
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 16),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16), color: Colors.white),
    );
  }
}

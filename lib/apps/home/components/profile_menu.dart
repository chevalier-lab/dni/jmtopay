import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/home/items/item_profile_menu.dart';
import 'package:jmtopay/components/custom_heading.dart';
import 'package:jmtopay/helper/str.dart';

class ProfileMenu extends StatelessWidget {
  final List<dynamic> items;
  final Function(dynamic) onItemClick;

  ProfileMenu({@required this.items, @required this.onItemClick});

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(16)),
        padding: EdgeInsets.all(8),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Container(
            child: SubHeading(label: STR.general),
            margin: EdgeInsets.all(8),
          ),
          Divider(),
          ListView.builder(
            itemCount: (this.items.length - 2),
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            padding: EdgeInsets.zero,
            itemBuilder: (context, index) {
              dynamic item = this.items[index];
              return Container(
                child: ItemProfileMenu(item: item, onClick: () {}),
                padding: EdgeInsets.all(8),
              );
            },
          )
        ]));
  }
}

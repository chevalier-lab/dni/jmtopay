import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/home/items/item_notification.dart';
import 'package:jmtopay/components/custom_heading.dart';

class NotificationList extends StatelessWidget {
  final String title;
  final List<dynamic> items;
  final Function(dynamic) onItemClick;
  final VoidCallback onLoadMore;

  NotificationList(
      {@required this.title,
      @required this.items,
      @required this.onItemClick,
      @required this.onLoadMore});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.white),
      padding: EdgeInsets.all(8),
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: SubHeading(label: this.title),
            margin: EdgeInsets.all(8),
          ),
          ListView.builder(
            itemCount: this.items.length,
            itemBuilder: (context, index) {
              dynamic item = items[index];
              return Container(
                child: ItemNotification(
                  item: item,
                  onClick: () {},
                ),
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
              );
            },
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
          )
        ],
      ),
    );
  }
}

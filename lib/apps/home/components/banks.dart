import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/components/custom_grid_view.dart';
import 'package:jmtopay/components/custom_heading.dart';
import 'package:jmtopay/components/custom_image.dart';
import 'package:jmtopay/helper/tint.dart';

class Banks extends StatelessWidget {
  final String title;
  final List<dynamic> items;
  final Function(dynamic) onItemClick;
  final VoidCallback onCollapse;

  Banks(
      {@required this.title,
      @required this.items,
      @required this.onItemClick,
      @required this.onCollapse});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(16)),
      padding: EdgeInsets.all(8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: SubHeading(label: this.title),
            margin: EdgeInsets.all(8),
          ),
          GridView.builder(
              gridDelegate: CustomGridView(crossAxisCount: 4, height: 108),
              itemCount: (items.length > 8) ? 8 : items.length,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              itemBuilder: (BuildContext ctx, int index) {
                dynamic item = items[index];
                return GestureDetector(
                  child: Container(
                    padding: EdgeInsets.all(8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        ImageSet(width: 48, height: 48, url: item["url"]),
                        Expanded(
                            child: Center(
                          child: AutoSizeText(
                            item["label"],
                            textAlign: TextAlign.center,
                            maxLines: 1,
                          ),
                        ))
                      ],
                    ),
                  ),
                  onTap: () => onItemClick(item),
                );
              }),
          (this.items.length > 8)
              ? Container(
                  width: MediaQuery.of(context).size.width,
                  child: FlatButton(
                    child: Text('See All'),
                    color: Tint.light,
                    textColor: Tint.text,
                    onPressed: () {},
                    padding: EdgeInsets.symmetric(vertical: 16),
                  ),
                  margin: EdgeInsets.all(8),
                )
              : Container()
        ],
      ),
    );
  }
}

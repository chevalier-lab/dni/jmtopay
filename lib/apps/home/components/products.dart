import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/home/items/item_product.dart';
import 'package:jmtopay/components/custom_heading.dart';
import 'package:jmtopay/helper/str.dart';

class Products extends StatelessWidget {
  final List<dynamic> items;
  final bool isLimited;
  final Function(dynamic) onItemClick;
  final VoidCallback onLoadMore;

  Products(
      {@required this.items,
      @required this.isLimited,
      @required this.onItemClick,
      @required this.onLoadMore});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.white),
      padding: EdgeInsets.all(8),
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: SubHeading(label: STR.products),
            margin: EdgeInsets.all(8),
          ),
          (this.items.length > 0)
              ? ListView.builder(
                  itemCount: this.items.length,
                  itemBuilder: (context, index) {
                    dynamic item = items[index];
                    return ItemProduct(
                      item: item,
                      isLast: index == this.items.length - 1,
                      onClick: () {
                        onItemClick(item);
                      },
                    );
                  },
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  padding: EdgeInsets.zero,
                )
              : Container(
                  alignment: Alignment.center,
                  child: Text("Data Not Found", textAlign: TextAlign.center))
        ],
      ),
    );
  }
}

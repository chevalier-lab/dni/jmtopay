import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/components/custom_grid_view.dart';
import 'package:jmtopay/components/custom_heading.dart';
import 'package:jmtopay/components/custom_image.dart';
import 'package:jmtopay/helper/img.dart';
import 'package:jmtopay/helper/str.dart';

class Services extends StatelessWidget {
  final List<dynamic> items;
  final bool isCollapse;
  final Function(dynamic) onItemClick;
  final VoidCallback onCollapse;

  Services(
      {@required this.items,
      @required this.isCollapse,
      @required this.onItemClick,
      @required this.onCollapse});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(16), topLeft: Radius.circular(16))),
      padding: EdgeInsets.all(8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: SubHeading(label: STR.services),
            margin: EdgeInsets.all(8),
          ),
          GridView.builder(
              gridDelegate: CustomGridView(crossAxisCount: 4, height: 100),
              itemCount: (items.length > 8) ? 8 : items.length,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              itemBuilder: (BuildContext ctx, int index) {
                if (index == 7 && !this.isCollapse)
                  return GestureDetector(
                    child: Container(
                      padding: EdgeInsets.all(8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ImageSet(width: 48, height: 48, url: IMG.more),
                          Expanded(
                              child: Center(
                            child: AutoSizeText(
                              STR.more,
                              textAlign: TextAlign.center,
                              maxLines: 1,
                            ),
                          ))
                        ],
                      ),
                    ),
                    onTap: () => onCollapse,
                  );

                dynamic item = items[index];
                return GestureDetector(
                  child: Container(
                    padding: EdgeInsets.all(8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        ImageSet(width: 48, height: 48, url: item["url"]),
                        Expanded(
                            child: Center(
                          child: AutoSizeText(
                            item["label"],
                            textAlign: TextAlign.center,
                            maxLines: 1,
                          ),
                        ))
                      ],
                    ),
                  ),
                  onTap: () => onItemClick(item),
                );
              }),
        ],
      ),
    );
  }
}

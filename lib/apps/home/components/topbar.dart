import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/helper/tint.dart';

class TopBar extends StatelessWidget {
  final String title;
  final List<Widget> actions;
  final bool isCanBeBack;

  final VoidCallback onBackPress;

  TopBar(
      {@required this.title,
      @required this.actions,
      @required this.isCanBeBack,
      @required this.onBackPress});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(16),
      child: Stack(
        children: [
          Container(
              width: MediaQuery.of(context).size.width,
              child: Text(
                this.title,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Tint.heading,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              )),
          GestureDetector(
            child: Icon(
              (this.isCanBeBack) ? Icons.chevron_left : Icons.close,
              color: Tint.label,
            ),
            onTap: () {
              if (this.isCanBeBack)
                this.onBackPress();
              else
                Navigator.of(context).pop();
            },
          ),
          Row(mainAxisAlignment: MainAxisAlignment.end, children: actions)
        ],
      ),
    );
  }
}

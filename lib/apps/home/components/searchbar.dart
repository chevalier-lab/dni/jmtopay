import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/components/custom_field.dart';
import 'package:jmtopay/components/custom_image.dart';
import 'package:jmtopay/helper/img.dart';
import 'package:jmtopay/models/home/wallet_md.dart';
import 'package:provider/provider.dart';

class SearchBar extends StatelessWidget {
  final Function(String) onSearch;

  SearchBar({@required this.onSearch});

  @override
  Widget build(BuildContext context) {
    final walletProvider = Provider.of<WalletMD>(context);

    return Container(
        padding: EdgeInsets.all(16),
        child: Row(
          children: [
            ImageSet(width: 48, height: 48, url: IMG.logo),
            SizedBox(width: 8),
            Expanded(
                child: FieldSearch(
                    field: walletProvider.search,
                    isAvalid: walletProvider.isValid,
                    onTyping: (_) {
                      walletProvider.checkIsValid();
                      this.onSearch(walletProvider.search["controller"].text);
                    },
                    onClear: () {
                      walletProvider.setClear();
                    }))
          ],
        ));
  }
}

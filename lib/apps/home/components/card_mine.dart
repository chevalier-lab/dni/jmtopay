import 'package:flutter/cupertino.dart';
import 'package:jmtopay/apps/home/items/item_card_mine.dart';

class CardMine extends StatelessWidget {
  final List<dynamic> items;
  final Function(dynamic) onItemClick;
  final Function(dynamic) onDeleteItem;

  CardMine(
      {@required this.items,
      @required this.onItemClick,
      @required this.onDeleteItem});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 240.0,
      child: ListView.builder(
        itemCount: this.items.length,
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.zero,
        itemBuilder: (context, index) {
          final dynamic item = this.items[index];
          return Container(
              child: ItemCardMine(
                item: item,
                onClick: () {
                  onItemClick(item);
                },
                onUnBinding: () {
                  onDeleteItem(item);
                },
              ),
              margin: EdgeInsets.only(left: 8, right: 8, bottom: 8));
        },
      ),
    );
  }
}

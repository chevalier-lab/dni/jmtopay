import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/components/custom_image.dart';
import 'package:jmtopay/helper/img.dart';
import 'package:jmtopay/helper/tint.dart';

class CardFull extends StatelessWidget {
  final String icon, fullName, validDate;
  final List<String> cardNumber;
  final VoidCallback onUnBinding;

  CardFull(
      {@required this.icon,
      @required this.fullName,
      @required this.validDate,
      @required this.cardNumber,
      @required this.onUnBinding});

  @override
  Widget build(BuildContext context) {
    final double cardFullWidth = MediaQuery.of(context).size.width;
    return Container(
      child: Stack(
        children: [
          ImageSet(
              width: cardFullWidth,
              height: cardFullWidth * 0.49,
              url: IMG.cardFull),
          Column(
            children: [
              Container(
                  child: Row(
                    children: [
                      ImageSet(width: 48, height: 36, url: IMG.cardChip),
                      Spacer(),
                      ImageSet(width: 42, height: 42, url: icon),
                    ],
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 32, vertical: 16)),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 32),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: List.generate(
                      cardNumber.length,
                      (index) => Text(
                            cardNumber[index],
                            style: TextStyle(
                                color: Tint.heading,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          )),
                ),
              ),
              SizedBox(height: 32),
              Container(
                child: Row(
                  children: [
                    Text(
                      fullName,
                      style: TextStyle(
                        color: Tint.heading,
                        fontSize: 14,
                      ),
                    ),
                    Spacer(),
                    Text(
                      validDate,
                      style: TextStyle(
                        color: Tint.field,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
                padding: EdgeInsets.symmetric(horizontal: 32),
              )
            ],
          ),
          Positioned(
              child: GestureDetector(
                onTap: () {
                  onUnBinding();
                },
                child: Icon(Icons.delete, color: Tint.danger, size: 32),
              ),
              top: 16,
              right: 16)
        ],
      ),
      margin: EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          border: Border.all(width: 1, color: Tint.light)),
    );
  }
}

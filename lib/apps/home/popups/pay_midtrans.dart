import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:jmtopay/helper/tint.dart';

class PayMidtrans extends StatefulWidget {
  PayMidtrans({@required this.redirectURL});

  final dynamic redirectURL;

  @override
  State createState() => PayMidtransscreen();
}

class PayMidtransscreen extends State<PayMidtrans> {
  double width;
  int index = 1;

  List<dynamic> listKurir = new List();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;

    return Scaffold(
        backgroundColor: Tint.white,
        appBar: AppBar(
          centerTitle: true,
          title: Text("Proses Pembayaran"),
          backgroundColor: Tint.main,
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
          ),
        ),
        body: IndexedStack(index: index, children: [
          InAppWebView(
            initialUrl: widget.redirectURL,
            initialOptions: InAppWebViewGroupOptions(
                crossPlatform: InAppWebViewOptions(
                    debuggingEnabled: true,
                    javaScriptEnabled: true,
                    userAgent:
                        "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0")),
            onWebViewCreated: (InAppWebViewController webViewController) {
              setState(() {
                index = 0;
              });
            },
            shouldOverrideUrlLoading:
                (controller, shouldOverrideUrlLoadingRequest) async {
              var url = shouldOverrideUrlLoadingRequest.url;
              if (url.contains("jmpay.my.id")) Navigator.of(context).pop(true);
              return ShouldOverrideUrlLoadingAction.ALLOW;
            },
          ),
          Container(
              color: Colors.white,
              child: Center(
                child: CircularProgressIndicator(),
              )),
        ]));
  }
}

import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/home/injections/transactions.dart';
import 'package:jmtopay/apps/home/items/item_transaction_history.dart';
import 'package:jmtopay/helper/global.dart';
import 'package:jmtopay/helper/str.dart';
import 'package:jmtopay/helper/tint.dart';
import 'package:jmtopay/helper/url.dart';
import 'package:screenshot/screenshot.dart';
import 'package:syncfusion_flutter_barcodes/barcodes.dart';
import 'package:wc_flutter_share/wc_flutter_share.dart';

class DetailTransactionPopups extends StatefulWidget {
  final TransactionsInjections injections;
  final dynamic transaction;

  DetailTransactionPopups(
      {@required this.injections, @required this.transaction});

  @override
  State<StatefulWidget> createState() => _DetailTransactionPopups();
}

class _DetailTransactionPopups extends State<DetailTransactionPopups> {
  ScreenshotController screenshotController = ScreenshotController();

  dynamic transaction;

  TransactionsInjections injections = new TransactionsInjections();

  num amountPirce = 0;
  num totalPrice = 0;

  @override
  void initState() {
    super.initState();

    transaction = widget.transaction;
    injections.initialize(context);

    setTotalPrice();

    // Load New Detail Transaction
    loadDetailTransaction();
  }

  void loadDetailTransaction() {
    injections.loadDetailTransaction(
        widget.transaction[RequestKey.transactionID],
        (newTransaction) => setState(() {
              transaction = newTransaction;
              setTotalPrice();
            }));
  }

  void setTotalPrice() {
    num amountPriceTemp = num.parse(transaction["amount_payment"]);
    num productPrice = num.parse(transaction[RequestKey.amount]);
    if (transaction["unit_payment"] == "1")
      setState(() {
        amountPirce = amountPriceTemp;
        totalPrice = productPrice + amountPriceTemp;
      });
    else {
      setState(() {
        amountPirce = (amountPriceTemp * productPrice) / 100.0;
        totalPrice = productPrice + ((amountPriceTemp * productPrice) / 100.0);
      });
    }
  }

  Widget setupPage() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 16),
        Container(
          alignment: Alignment.topCenter,
          child: Text(
            "JMTO Pay",
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        SizedBox(height: 8),
        Container(
          alignment: Alignment.topCenter,
          child: Text(
            transaction[RequestKey.merchantName],
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
          ),
        ),
        Container(
          alignment: Alignment.topCenter,
          child: Text(
            transaction[RequestKey.merchantAddress],
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
          ),
        ),
        SizedBox(height: 16),
        Container(
          alignment: Alignment.topCenter,
          child: Text(
            "Sale No: ${transaction[RequestKey.transactionNo]}",
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
          ),
        ),
        SizedBox(height: 8),
        Container(
          alignment: Alignment.topLeft,
          margin: EdgeInsets.symmetric(horizontal: 16),
          child: Text(
            "Date: ${transaction[RequestKey.updatedAt]}",
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 14),
          ),
        ),
        Container(
          alignment: Alignment.topLeft,
          margin: EdgeInsets.symmetric(horizontal: 16),
          child: Text(
            "Customer Name: ${transaction[RequestKey.cmName]}",
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 14),
          ),
        ),
        Container(
          alignment: Alignment.topLeft,
          margin: EdgeInsets.symmetric(horizontal: 16),
          child: Text(
            "Phone: ${transaction[RequestKey.phone]}",
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 14),
          ),
        ),
        Container(
          alignment: Alignment.topLeft,
          margin: EdgeInsets.symmetric(horizontal: 16),
          child: Text(
            "Email: ${transaction[RequestKey.email]}",
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 14),
          ),
        ),
        SizedBox(height: 8),
        Container(
          child: ItemTransactionHistory(
              item: transaction, isLast: true, onClick: () {}),
          margin: EdgeInsets.symmetric(horizontal: 8),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 8),
          child: Row(
            children: [
              Text("Biaya Administrasi: "),
              Spacer(),
              Text(formatCurrency.format(amountPirce))
            ],
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 8),
          child: Row(
            children: [
              Text("Total Tagihan: "),
              Spacer(),
              Text(formatCurrency.format(totalPrice))
            ],
          ),
        ),
        SizedBox(height: 8),
        Container(
          child:
              SfBarcodeGenerator(value: transaction[RequestKey.transactionNo]),
          margin: EdgeInsets.symmetric(horizontal: 8),
          height: 80,
        ),
        SizedBox(height: 8),
        Container(
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Tint.main,
                textStyle: const TextStyle(fontSize: 16),
                padding: EdgeInsets.symmetric(vertical: 16)),
            onPressed: () => {
              screenshotController
                  .capture(delay: const Duration(milliseconds: 30))
                  .then((Uint8List image) async {
                if (image != null) {
                  WcFlutterShare.share(
                      sharePopupTitle: "Share Invoice",
                      mimeType: 'image/png',
                      fileName: "share-invoice.png",
                      bytesOfFile: image);
                }
              })
            },
            child: Text("Bagikan Ke Social Media"),
          ),
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 8),
          color: Colors.white,
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(STR.detailTransaction)),
        backgroundColor: Tint.white,
        body: SafeArea(
          child: SingleChildScrollView(
              child: Screenshot(
            child: Container(
              child: setupPage(),
              color: Tint.white,
            ),
            controller: screenshotController,
          )),
        ));
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jmtopay/apps/auth/components/template.dart';
import 'package:jmtopay/components/custom_description.dart';
import 'package:jmtopay/components/custom_field.dart';
import 'package:jmtopay/components/custom_heading.dart';
import 'package:jmtopay/helper/cache.dart';
import 'package:jmtopay/helper/tint.dart';
import 'package:jmtopay/helper/url.dart';
import 'package:jmtopay/models/home_md.dart';

class OTPPage extends StatefulWidget {
  static const String pageName = "/otp";
  final HomeMD provider;

  OTPPage({@required this.provider});

  @override
  State<StatefulWidget> createState() => _OTPPage();
}

class _OTPPage extends State<OTPPage> {
  String fieldPIN = "", verificationID = "", phoneNumber = "";
  bool isFormValid = false, isCodeAlreadySend = false;
  List fields = [
    {
      "type": TextInputType.number,
      "format": FilteringTextInputFormatter.digitsOnly,
      "focusNode": FocusNode(),
      "value": ""
    },
    {
      "type": TextInputType.number,
      "format": FilteringTextInputFormatter.digitsOnly,
      "focusNode": FocusNode(),
      "value": ""
    },
    {
      "type": TextInputType.number,
      "format": FilteringTextInputFormatter.digitsOnly,
      "focusNode": FocusNode(),
      "value": ""
    },
    {
      "type": TextInputType.number,
      "format": FilteringTextInputFormatter.digitsOnly,
      "focusNode": FocusNode(),
      "value": ""
    },
    {
      "type": TextInputType.number,
      "format": FilteringTextInputFormatter.digitsOnly,
      "focusNode": FocusNode(),
      "value": ""
    },
    {
      "type": TextInputType.number,
      "format": FilteringTextInputFormatter.digitsOnly,
      "focusNode": FocusNode(),
      "value": ""
    },
  ];

  @override
  void initState() {
    super.initState();

    setupPhoneNumber();
  }

  void setupPhoneNumber() async {
    final auth = await getAuth();
    setState(() {
      phoneNumber = auth[RequestKey.phone];
    });
  }

  // Set PIN
  void setPIN(String fieldPIN) {
    this.fieldPIN = fieldPIN;
    checkIsFormValid();
  }

  // Set Fields Value
  void setFieldsValue(String value, int position) {
    setState(() {
      this.fields[position]["value"] = value;
    });
  }

  // Cehck is Form Valid
  void checkIsFormValid() {
    var temp = false;
    if (this.fieldPIN.length == 6)
      temp = true;
    else
      temp = false;
    setState(() {
      this.isFormValid = temp;
    });
  }

  // Check is PIN Valid
  bool checkIsPINValid() => (this.fieldPIN.length == 6);

  // Setup Page
  Widget setupPage(HomeMD provider) {
    return Column(
      children: [
        Heading(label: provider.content["title"][OTPPage.pageName]["label"]),
        SizedBox(height: 16),
        Description(
            label: provider.content["description"][OTPPage.pageName]["label"]),
        SizedBox(height: 32),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            children: [
              Expanded(
                  child: FieldPIN(
                field: this.fields[0],
                isAvalid: (this.fields[0]["value"] != ""),
                onTyping: (value) {
                  if (value.length == 0) {
                    this.setFieldsValue("", 0);
                    return;
                  }
                  if (this.fields[0]["value"].length >= 1) {
                    this.fields[1]["focusNode"].requestFocus();
                  } else {
                    if (value.length == 1) {
                      this.setFieldsValue(value, 0);
                      this.fields[1]["focusNode"].requestFocus();
                    } else {
                      this.setFieldsValue("", 0);
                    }
                  }
                },
              )),
              SizedBox(width: 8),
              Expanded(
                  child: FieldPIN(
                field: this.fields[1],
                isAvalid: (this.fields[1]["value"] != ""),
                onTyping: (value) {
                  if (value.length == 0) {
                    this.setFieldsValue("", 1);
                    this.fields[0]["focusNode"].requestFocus();
                    return;
                  }
                  if (this.fields[1]["value"].length >= 1) {
                    this.fields[2]["focusNode"].requestFocus();
                  } else {
                    if (value.length == 1) {
                      this.setFieldsValue(value, 1);
                      this.fields[2]["focusNode"].requestFocus();
                    } else {
                      this.setFieldsValue("", 2);
                      this.fields[1]["focusNode"].requestFocus();
                    }
                  }
                },
              )),
              SizedBox(width: 8),
              Expanded(
                  child: FieldPIN(
                field: this.fields[2],
                isAvalid: (this.fields[2]["value"] != ""),
                onTyping: (value) {
                  if (value.length == 0) {
                    this.setFieldsValue("", 2);
                    this.fields[1]["focusNode"].requestFocus();
                    return;
                  }
                  if (this.fields[2]["value"].length >= 1) {
                    this.fields[3]["focusNode"].requestFocus();
                  } else {
                    if (value.length == 1) {
                      this.setFieldsValue(value, 2);
                      this.fields[3]["focusNode"].requestFocus();
                    } else {
                      this.setFieldsValue("", 2);
                      this.fields[1]["focusNode"].requestFocus();
                    }
                  }
                },
              )),
              SizedBox(width: 8),
              Expanded(
                  child: FieldPIN(
                field: this.fields[3],
                isAvalid: (this.fields[3]["value"] != ""),
                onTyping: (value) {
                  if (value.length == 0) {
                    this.setFieldsValue("", 3);
                    this.fields[2]["focusNode"].requestFocus();
                    return;
                  }
                  if (this.fields[3]["value"].length >= 1) {
                    this.setPIN(this.fields.map((e) => e["value"]).join(""));
                  } else {
                    if (value.length == 1) {
                      this.setFieldsValue(value, 3);
                      this.setPIN(this.fields.map((e) => e["value"]).join(""));
                      this.fields[4]["focusNode"].requestFocus();
                    } else {
                      this.setFieldsValue("", 3);
                      this.fields[2]["focusNode"].requestFocus();
                    }
                  }
                },
              )),
              SizedBox(width: 8),
              Expanded(
                  child: FieldPIN(
                field: this.fields[4],
                isAvalid: (this.fields[4]["value"] != ""),
                onTyping: (value) {
                  if (value.length == 0) {
                    this.setFieldsValue("", 4);
                    this.fields[3]["focusNode"].requestFocus();
                    return;
                  }
                  if (this.fields[4]["value"].length >= 1) {
                    this.setPIN(this.fields.map((e) => e["value"]).join(""));
                  } else {
                    if (value.length == 1) {
                      this.setFieldsValue(value, 4);
                      this.setPIN(this.fields.map((e) => e["value"]).join(""));
                      this.fields[5]["focusNode"].requestFocus();
                    } else {
                      this.setFieldsValue("", 4);
                      this.fields[3]["focusNode"].requestFocus();
                    }
                  }
                },
              )),
              SizedBox(width: 8),
              Expanded(
                  child: FieldPIN(
                field: this.fields[5],
                isAvalid: (this.fields[5]["value"] != ""),
                onTyping: (value) {
                  if (value.length == 0) {
                    this.setFieldsValue("", 5);
                    this.fields[4]["focusNode"].requestFocus();
                    return;
                  }
                  if (this.fields[5]["value"].length >= 1) {
                    this.setPIN(this.fields.map((e) => e["value"]).join(""));
                    FocusScope.of(context).unfocus();
                  } else {
                    if (value.length == 1) {
                      this.setFieldsValue(value, 5);
                      this.setPIN(this.fields.map((e) => e["value"]).join(""));
                      FocusScope.of(context).unfocus();
                    } else {
                      this.setFieldsValue("", 5);
                      this.fields[4]["focusNode"].requestFocus();
                    }
                  }
                },
              ))
            ],
          ),
        ),
      ],
    );
  }

  // Do Send OTP
  void doSendOTP() {
    Navigator.of(context)
        .pop({"status": true, "message": "Do send otp", "pin": fieldPIN});
  }

  // Render Page
  @override
  Widget build(BuildContext context) {
    final provider = widget.provider;

    return Scaffold(
        backgroundColor: Tint.background,
        body: SafeArea(
            child: Template(
          onClose: () => Navigator.of(context).pop(),
          currentPage: OTPPage.pageName,
          view: setupPage(provider),
          actionTop: Container(),
        )),
        bottomNavigationBar: Column(mainAxisSize: MainAxisSize.min, children: [
          Container(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: (this.isFormValid) ? Tint.main : Tint.disabled,
                  textStyle: const TextStyle(fontSize: 16),
                  padding: EdgeInsets.symmetric(vertical: 16)),
              onPressed: () => {if (this.isFormValid) doSendOTP() else null},
              child: Text(provider.content["buttons"][11]),
            ),
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(16),
            color: Colors.white,
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(children: [
                  TextSpan(
                    text: provider.content["buttons"][5] + "\n",
                    style: TextStyle(color: Tint.text),
                  ),
                  TextSpan(
                      text: provider.content["buttons"][7] + " ",
                      style: TextStyle(color: Tint.main)),
                ])),
            padding: EdgeInsets.only(bottom: 16),
            color: Colors.white,
          ),
        ]));
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jmtopay/apps/home/injections/cards.dart';
import 'package:jmtopay/apps/home/injections/payments.dart';
import 'package:jmtopay/apps/home/injections/products.dart';
import 'package:jmtopay/apps/home/injections/transactions.dart';
import 'package:jmtopay/apps/home/items/item_payment_method.dart';
import 'package:jmtopay/apps/home/items/item_product.dart';
import 'package:jmtopay/apps/home/popups/pay_midtrans.dart';
import 'package:jmtopay/components/custom_field.dart';
import 'package:jmtopay/helper/global.dart';
import 'package:jmtopay/helper/sheet.dart';
import 'package:jmtopay/helper/str.dart';
import 'package:jmtopay/helper/tint.dart';
import 'package:jmtopay/helper/url.dart';

class DetailProductPopups extends StatefulWidget {
  final ProductsInjections injections;
  final dynamic product;

  DetailProductPopups({@required this.injections, @required this.product});

  @override
  State<StatefulWidget> createState() => _DetailProductPopups();
}

class _DetailProductPopups extends State<DetailProductPopups> {
  List<dynamic> listPaymentMethod = [];
  int selectedListPaymentMethod = 0;
  String fieldNote = "";
  num amountPirce = 0;
  num totalPrice = 0;

  final transactionInjections = TransactionsInjections();
  final cardInjections = CardInjections();
  final paymentInjections = PaymentsInjections();

  @override
  void initState() {
    super.initState();

    transactionInjections.initialize(context);
    cardInjections.initialize(context);
    paymentInjections.initialize(context);

    setupListPaymentMethod();
  }

  void setupListPaymentMethod() async {
    paymentInjections.loadPayments((payments) {
      cardInjections.checkBinding((List<dynamic> data) {
        int indexOfBRI = -1;
        int indexOfMidtrans = -1;
        int indexOfLinkAja = -1;

        int index = 0;
        payments.forEach((element) {
          if (element["payment_name"].toString().contains("BRI"))
            indexOfBRI = index;
          else if (element["payment_name"].toString().contains("Midtrans") ||
              element["payment_name"].toString().contains("Gopay"))
            indexOfMidtrans = index;
          else if (element["payment_name"].toString().contains("LinkAja"))
            indexOfLinkAja = index;
          index += 1;
        });

        List<dynamic> temp = [];
        data.forEach((dynamic item) {
          if (item[RequestKey.bindingStatus] == "DONE") {
            if (item[RequestKey.cardPan].toString().length == 16)
              temp.add({
                "icon": payments[indexOfBRI]["link_image"],
                "label": "BRI (${item['card_pan']})",
                "binding_id": item[RequestKey.bindingID],
                "amount": payments[indexOfBRI]["amount"],
                "unit": payments[indexOfBRI]["unit"]
              });
            else
              temp.add({
                "icon": payments[indexOfLinkAja]["link_image"],
                "label":
                    "${payments[indexOfLinkAja]['payment_name']} (${item['phone_number']})",
                "binding_id": item[RequestKey.bindingID],
                "amount": payments[indexOfLinkAja]["amount"],
                "unit": payments[indexOfLinkAja]["unit"]
              });
          }
        });

        temp.add({
          "icon": payments[indexOfMidtrans]["link_image"],
          "label": payments[indexOfMidtrans]["payment_name"],
          "binding_id": "",
          "amount": payments[indexOfMidtrans]["amount"],
          "unit": payments[indexOfMidtrans]["unit"]
        });
        setState(() {
          listPaymentMethod = temp;
        });

        setTotalPrice();
      });
    });
  }

  void orderTransactionFromBank() {
    transactionInjections.doOrder(
        widget.product[RequestKey.productID],
        listPaymentMethod[selectedListPaymentMethod]["binding_id"],
        fieldNote,
        () => Navigator.of(context)
            .pop({"status": true, "message": "Success to order"}));
  }

  void orderTransactionFromMidtrans() {
    transactionInjections.doOrderMidtrans(widget.product[RequestKey.productID],
        fieldNote, (data) => openPaymentMidtrans(context, data));
  }

  void orderTransactionFromLinkAja() {
    transactionInjections.doOrderLinkAja(
        widget.product[RequestKey.productID],
        listPaymentMethod[selectedListPaymentMethod]["binding_id"],
        fieldNote,
        (message) =>
            Navigator.of(context).pop({"status": true, "message": message}));
  }

  // Open Payment Midtrans
  Future openPaymentMidtrans(BuildContext context, dynamic data) async {
    var response =
        await Navigator.of(context).push(new MaterialPageRoute<dynamic>(
            builder: (BuildContext context) {
              return new PayMidtrans(redirectURL: data["data"]);
            },
            fullscreenDialog: true));
    if (response != null) {
      if (response) {
        Navigator.of(context)
            .pop({"status": true, "message": "Success to order"});
      } else
        Navigator.of(context)
            .pop({"status": true, "message": data[RequestKey.message]});
    } else
      Navigator.of(context)
          .pop({"status": true, "message": data[RequestKey.message]});
  }

  // Order Transaction
  void doOrderTransaction() {
    if (listPaymentMethod[selectedListPaymentMethod]["label"]
        .toString()
        .contains("BRI"))
      orderTransactionFromBank();
    else if (listPaymentMethod[selectedListPaymentMethod]["label"]
            .toString()
            .contains("Midtrans") ||
        listPaymentMethod[selectedListPaymentMethod]["label"]
            .toString()
            .contains("Gopay"))
      orderTransactionFromMidtrans();
    else if (listPaymentMethod[selectedListPaymentMethod]["label"]
        .toString()
        .contains("LinkAja")) orderTransactionFromLinkAja();
  }

  void setTotalPrice() {
    if (listPaymentMethod.length > 0) {
      num amountPriceTemp =
          num.parse(listPaymentMethod[selectedListPaymentMethod]["amount"]);
      num productPrice = num.parse(widget.product[RequestKey.amount]);
      if (listPaymentMethod[selectedListPaymentMethod]["unit"] == "1")
        setState(() {
          amountPirce = amountPriceTemp;
          totalPrice = productPrice + amountPriceTemp;
        });
      else {
        setState(() {
          amountPirce = (amountPriceTemp * productPrice) / 100.0;
          totalPrice =
              productPrice + ((amountPriceTemp * productPrice) / 100.0);
        });
      }
    }
  }

  Widget setupPage() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 8, left: 8, right: 8),
          child:
              ItemProduct(item: widget.product, isLast: false, onClick: () {}),
        ),
        Padding(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: Row(children: [
              Text("Category: ", style: TextStyle(fontWeight: FontWeight.bold)),
              Expanded(child: Text(widget.product[RequestKey.categoryName]))
            ])),
        Padding(
            padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
            child: Row(children: [
              Text("Address: ", style: TextStyle(fontWeight: FontWeight.bold)),
              Expanded(child: Text(widget.product[RequestKey.merchantAddress]))
            ])),
        Padding(
          padding: EdgeInsets.only(left: 16, right: 16, bottom: 8),
          child: Text("Payment Method: ",
              style: TextStyle(fontWeight: FontWeight.bold)),
        ),
        Padding(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: (listPaymentMethod.length > 0)
                ? ListView.builder(
                    itemCount: listPaymentMethod.length,
                    itemBuilder: (context, index) {
                      dynamic item = listPaymentMethod[index];
                      return ItemPaymentMethod(
                        item: item,
                        isActive: (index == selectedListPaymentMethod),
                        onClick: () {
                          setState(() {
                            selectedListPaymentMethod = index;
                          });
                          setTotalPrice();
                        },
                      );
                    },
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    padding: EdgeInsets.zero,
                  )
                : Container(
                    padding: EdgeInsets.all(8),
                    width: MediaQuery.of(context).size.width,
                    child: Text("Belum ada kartu yang terkait"),
                    color: Tint.disabled,
                  )),
        Padding(
          padding: EdgeInsets.only(left: 16, right: 16, bottom: 8, top: 8),
          child: Text("Note: ", style: TextStyle(fontWeight: FontWeight.bold)),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Textarea(
              field: {
                "label": "Note",
                "hint": "Please enter your note",
                "type": TextInputType.text,
                "format": FilteringTextInputFormatter.singleLineFormatter
              },
              onTyping: (value) => setState(() {
                    fieldNote = value;
                  })),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(STR.detailProduct)),
        body: SafeArea(
          child: SingleChildScrollView(child: setupPage()),
        ),
        bottomNavigationBar: Column(mainAxisSize: MainAxisSize.min, children: [
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 8),
            child: Row(
              children: [
                Text("Biaya Administrasi: "),
                Spacer(),
                Text(formatCurrency.format(amountPirce))
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 8),
            child: Row(
              children: [
                Text("Total Tagihan: "),
                Spacer(),
                Text(formatCurrency.format(totalPrice))
              ],
            ),
          ),
          Container(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Tint.main,
                  textStyle: const TextStyle(fontSize: 16),
                  padding: EdgeInsets.symmetric(vertical: 16)),
              onPressed: () => {
                if (listPaymentMethod.length > 0)
                  doOrderTransaction()
                else
                  error(context, "Belum ada kartu yang terkait")
              },
              child: Text("Order"),
            ),
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 8),
            color: Colors.white,
          )
        ]));
  }
}

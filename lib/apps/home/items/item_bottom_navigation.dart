import 'package:flutter/cupertino.dart';
import 'package:jmtopay/helper/tint.dart';

BottomNavigationBarItem itemButtonNavigation(
        dynamic item, VoidCallback onClick) =>
    BottomNavigationBarItem(
        icon: GestureDetector(
          child: Container(
              decoration: BoxDecoration(
                color: (item["is_center"]) ? Tint.main : Tint.white,
                borderRadius: BorderRadius.circular(16),
              ),
              padding: EdgeInsets.all(8),
              child: Icon(
                item["icon"],
                size: 28,
                color: (item["is_center"])
                    ? Tint.white
                    : (item["is_active"])
                        ? Tint.main
                        : Tint.label,
              )),
          onTap: onClick,
        ),
        label: "");

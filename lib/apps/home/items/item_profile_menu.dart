import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/helper/tint.dart';

class ItemProfileMenu extends StatelessWidget {
  final dynamic item;
  final VoidCallback onClick;

  ItemProfileMenu({@required this.item, @required this.onClick});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Icon(item["icon"], color: item["color"]),
          SizedBox(width: 8),
          Expanded(
              child: Text(item["label"],
                  style: TextStyle(color: Tint.heading, fontSize: 14))),
          SizedBox(width: 8),
          Icon(
            Icons.chevron_right_outlined,
            color: Tint.field,
          )
        ],
      ),
      onTap: onClick,
    );
  }
}

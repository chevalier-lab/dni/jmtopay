import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/components/custom_image.dart';
import 'package:jmtopay/helper/global.dart';
import 'package:jmtopay/helper/img.dart';
import 'package:jmtopay/helper/tint.dart';

class ItemCardMine extends StatelessWidget {
  final dynamic item;
  final VoidCallback onClick;
  final VoidCallback onUnBinding;

  ItemCardMine(
      {@required this.item,
      @required this.onClick,
      @required this.onUnBinding});

  @override
  Widget build(BuildContext context) {
    final double width = 60;
    return Container(
      width: width * 3,
      height: width * 4,
      padding: EdgeInsets.all(8),
      child: Stack(
        children: [
          ImageSet(width: width * 3, height: width * 4, url: IMG.cardThumb),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  child: Row(
                    children: [
                      ImageSet(width: 48, height: 36, url: IMG.cardChip),
                      Spacer(),
                      Text(
                          "**** ${item['card_number'][item['card_number'].length - 1]}")
                    ],
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16)),
              SizedBox(height: 42),
              Container(
                  child: ImageSet(width: 42, height: 42, url: item["icon"]),
                  padding: EdgeInsets.symmetric(horizontal: 16)),
              Container(
                  child: Text(
                    formatCurrency.format(item["balance"]),
                    style: TextStyle(
                        color: Tint.heading,
                        fontSize: 14,
                        fontWeight: FontWeight.bold),
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16))
            ],
          ),
          Positioned(
              child: GestureDetector(
                onTap: () {
                  onUnBinding();
                },
                child: Icon(Icons.delete, color: Tint.danger, size: 32),
              ),
              top: 16,
              right: 16)
        ],
      ),
    );
  }
}

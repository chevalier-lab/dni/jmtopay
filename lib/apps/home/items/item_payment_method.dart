import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/components/custom_image.dart';
import 'package:jmtopay/helper/tint.dart';

class ItemPaymentMethod extends StatelessWidget {
  final dynamic item;
  final bool isActive;
  final VoidCallback onClick;

  ItemPaymentMethod(
      {@required this.item, @required this.isActive, @required this.onClick});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        padding: EdgeInsets.all(8),
        color: (isActive) ? Tint.disabled : Tint.light,
        child: Column(
          children: [
            Row(
              children: [
                ImageNetwork(width: 32, height: 32, url: item["icon"]),
                SizedBox(width: 16),
                Expanded(
                    child: Text(
                  item["label"],
                  style: TextStyle(
                      color: Tint.heading,
                      fontSize: 14,
                      fontWeight: FontWeight.bold),
                )),
                SizedBox(width: 16),
              ],
            ),
          ],
        ),
      ),
      onTap: onClick,
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:jmtopay/components/custom_image.dart';
import 'package:jmtopay/helper/tint.dart';

class ItemNotification extends StatelessWidget {
  final dynamic item;
  final VoidCallback onClick;

  ItemNotification({@required this.item, @required this.onClick});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ImageSet(width: 48, height: 48, url: item["icon"]),
        SizedBox(width: 16),
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(item["label"],
                textAlign: TextAlign.start,
                style: TextStyle(fontSize: 14, color: Tint.field)),
            Text(item["description"],
                textAlign: TextAlign.start,
                style: TextStyle(
                    fontSize: 12,
                    color: (item["type"] == "in") ? Tint.success : Tint.text)),
          ],
        ))
      ],
    );
  }
}

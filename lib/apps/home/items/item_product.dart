import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/components/custom_image.dart';
import 'package:jmtopay/helper/global.dart';
import 'package:jmtopay/helper/img.dart';
import 'package:jmtopay/helper/tint.dart';
import 'package:jmtopay/helper/url.dart';

class ItemProduct extends StatelessWidget {
  final dynamic item;
  final bool isLast;
  final VoidCallback onClick;

  ItemProduct(
      {@required this.item, @required this.isLast, @required this.onClick});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        padding: EdgeInsets.all(8),
        child: Column(
          children: [
            Row(
              children: [
                ImageSet(width: 48, height: 48, url: IMG.car),
                SizedBox(width: 16),
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      item[RequestKey.productName],
                      style: TextStyle(
                          color: Tint.heading,
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      item[RequestKey.merchantName],
                      style: TextStyle(
                        color: Tint.text,
                        fontSize: 12,
                      ),
                    ),
                  ],
                )),
                SizedBox(width: 16),
                Container(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      formatCurrency.format(num.parse(item[RequestKey.amount])),
                      style: TextStyle(
                          color: Tint.heading,
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      "/ " + item[RequestKey.unit],
                      style: TextStyle(
                        color: Tint.text,
                        fontSize: 12,
                      ),
                    ),
                  ],
                )),
              ],
            ),
            SizedBox(height: 16),
            (isLast) ? Container() : Divider()
          ],
        ),
      ),
      onTap: onClick,
    );
  }
}

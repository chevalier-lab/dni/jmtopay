import 'package:flutter/cupertino.dart';
import 'package:jmtopay/apps/splashscreen/view.dart';
import 'package:jmtopay/models/splashscreen_md.dart';
import 'package:provider/provider.dart';

class SplashScreenProvider {
  static final String routeName = "/";

  static Widget init() => ChangeNotifierProvider<SplashScreenMD>(
        create: (context) => SplashScreenMD(),
        child: SplashScreenView(),
      );
}

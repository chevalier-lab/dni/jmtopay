import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/components/custom_description.dart';
import 'package:jmtopay/components/custom_heading.dart';
import 'package:jmtopay/components/custom_image.dart';

class SliderItem extends StatelessWidget {
  final dynamic item;

  SliderItem({@required this.item});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ImageSet(width: 305, height: 253, url: item["ilustration"]),
        SizedBox(height: 64),
        Heading(label: item["title"]),
        SizedBox(height: 16),
        Description(label: item["description"]),
        SizedBox(height: 24),
      ],
    );
  }
}

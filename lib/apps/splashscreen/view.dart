import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/auth/provider.dart';
import 'package:jmtopay/apps/home/provider.dart';
import 'package:jmtopay/apps/splashscreen/components/slider_item.dart';
import 'package:jmtopay/components/custom_image.dart';
import 'package:jmtopay/helper/cache.dart';
import 'package:jmtopay/helper/img.dart';
import 'package:jmtopay/helper/tint.dart';
import 'package:jmtopay/models/splashscreen_md.dart';
import 'package:provider/provider.dart';

class SplashScreenView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SplashScreenView();
}

class _SplashScreenView extends State<SplashScreenView> {
  dynamic auth;

  @override
  void initState() {
    super.initState();

    setupAuth();
  }

  void setupAuth() async {
    auth = await getAuth();
    if (auth != null) {
      Navigator.of(context).pushReplacementNamed(HomeProvider.routeName);
    }
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<SplashScreenMD>(context);

    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          children: [
            SizedBox(height: 16),
            ImageSet(width: 159, height: 45, url: IMG.logoHorizontal),
            SliderItem(item: provider.getCurrentSlider),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(provider.getSize, (index) {
                return Container(
                  padding: EdgeInsets.symmetric(vertical: 4, horizontal: 4),
                  color: (provider.currentPosition == index)
                      ? Tint.main
                      : Tint.disabled,
                  margin: EdgeInsets.symmetric(horizontal: 2),
                );
              }),
            )
          ],
        ),
      ),
      bottomNavigationBar: Container(
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              textStyle: const TextStyle(fontSize: 16),
              padding: EdgeInsets.symmetric(vertical: 16)),
          onPressed: () => {
            if (provider.currentPosition == (provider.getSize - 1))
              Navigator.pushReplacementNamed(context, AuthProvider.routeName)
            else
              provider.setNext()
          },
          child: const Text('Get Started'),
        ),
        margin: EdgeInsets.all(16),
      ),
    );
  }
}

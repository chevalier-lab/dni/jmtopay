import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/components/custom_image.dart';
import 'package:jmtopay/helper/img.dart';
import 'package:jmtopay/helper/tint.dart';

class Template extends StatelessWidget {
  final Widget view, actionTop;
  final String currentPage;
  final VoidCallback onClose;

  Template(
      {@required this.view,
      @required this.currentPage,
      @required this.actionTop,
      @required this.onClose});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 16),
        Container(
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            children: [
              GestureDetector(
                child: Icon(Icons.close, color: Tint.heading),
                onTap: onClose,
              ),
              Spacer(),
              actionTop
            ],
          ),
        ),
        SizedBox(height: 16),
        Expanded(
            child: Stack(
          children: [
            Positioned(
                child: Container(
                  margin: EdgeInsets.only(top: 42),
                  padding: EdgeInsets.only(top: 84),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(32),
                        topRight: Radius.circular(32),
                      ),
                      color: Colors.white),
                  child: SingleChildScrollView(child: this.view),
                ),
                top: 0,
                bottom: 0,
                left: 0,
                right: 0),
            Align(
              child: Container(
                child: ImageSet(width: 84, height: 84, url: IMG.logo),
                padding: EdgeInsets.all(8),
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.white),
              ),
              alignment: Alignment.topCenter,
            )
          ],
        ))
      ],
    );
  }
}

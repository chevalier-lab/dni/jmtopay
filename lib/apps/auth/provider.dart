import 'package:flutter/cupertino.dart';
import 'package:jmtopay/apps/auth/view.dart';
import 'package:jmtopay/models/Auth_md.dart';
import 'package:provider/provider.dart';

class AuthProvider {
  static final String routeName = "/auth";

  static Widget init() => ChangeNotifierProvider<AuthMD>(
        create: (context) => AuthMD(),
        child: AuthView(),
      );
}

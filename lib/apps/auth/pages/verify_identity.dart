import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/auth/components/template.dart';
import 'package:jmtopay/apps/auth/pages/login.dart';
import 'package:jmtopay/apps/auth/pages/reset_password.dart';
import 'package:jmtopay/components/custom_description.dart';
import 'package:jmtopay/components/custom_field.dart';
import 'package:jmtopay/components/custom_heading.dart';
import 'package:jmtopay/helper/tint.dart';
import 'package:jmtopay/models/Auth_md.dart';
import 'package:jmtopay/models/auth/verify_identity_md.dart';
import 'package:provider/provider.dart';

class VerifyIdentityPageProvider {
  static const String pageName = "/verify-identity";

  static Widget init(BuildContext context) {
    return ChangeNotifierProvider<VerifyIdentityMD>(
        create: (_) => VerifyIdentityMD(), child: VerifyIdentityPage());
  }
}

class VerifyIdentityPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _VerifyIdentityPage();
}

class _VerifyIdentityPage extends State<VerifyIdentityPage> {
  Widget setupPage(VerifyIdentityMD verifyIdentityProvider, provider) {
    return Column(
      children: [
        Heading(
            label: provider.content["title"]
                [VerifyIdentityPageProvider.pageName]["label"]),
        SizedBox(height: 16),
        Description(
            label: provider.content["description"]
                [VerifyIdentityPageProvider.pageName]["label"]),
        SizedBox(height: 32),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            children: [
              Expanded(
                  child: FieldPIN(
                field: verifyIdentityProvider.fields[0],
                isAvalid: (verifyIdentityProvider.fields[0]["value"] != ""),
                onTyping: (value) {
                  if (value.length == 0) {
                    verifyIdentityProvider.setFieldsValue("", 0);
                    checkIsAlreadyFill(verifyIdentityProvider);
                    return;
                  }
                  if (verifyIdentityProvider.fields[0]["value"].length >= 1) {
                    verifyIdentityProvider.fields[1]["focusNode"]
                        .requestFocus();
                    checkIsAlreadyFill(verifyIdentityProvider);
                  } else {
                    if (value.length == 1) {
                      verifyIdentityProvider.setFieldsValue(value, 0);
                      verifyIdentityProvider.fields[1]["focusNode"]
                          .requestFocus();
                      checkIsAlreadyFill(verifyIdentityProvider);
                    } else {
                      verifyIdentityProvider.setFieldsValue("", 0);
                      checkIsAlreadyFill(verifyIdentityProvider);
                    }
                  }
                },
              )),
              SizedBox(width: 8),
              Expanded(
                  child: FieldPIN(
                field: verifyIdentityProvider.fields[1],
                isAvalid: (verifyIdentityProvider.fields[1]["value"] != ""),
                onTyping: (value) {
                  if (value.length == 0) {
                    verifyIdentityProvider.setFieldsValue("", 1);
                    verifyIdentityProvider.fields[0]["focusNode"]
                        .requestFocus();
                    checkIsAlreadyFill(verifyIdentityProvider);
                    return;
                  }
                  if (verifyIdentityProvider.fields[1]["value"].length >= 1) {
                    verifyIdentityProvider.fields[2]["focusNode"]
                        .requestFocus();
                    checkIsAlreadyFill(verifyIdentityProvider);
                  } else {
                    if (value.length == 1) {
                      verifyIdentityProvider.setFieldsValue(value, 1);
                      verifyIdentityProvider.fields[2]["focusNode"]
                          .requestFocus();
                      checkIsAlreadyFill(verifyIdentityProvider);
                    } else {
                      verifyIdentityProvider.setFieldsValue("", 2);
                      verifyIdentityProvider.fields[1]["focusNode"]
                          .requestFocus();
                      checkIsAlreadyFill(verifyIdentityProvider);
                    }
                  }
                },
              )),
              SizedBox(width: 8),
              Expanded(
                  child: FieldPIN(
                field: verifyIdentityProvider.fields[2],
                isAvalid: (verifyIdentityProvider.fields[2]["value"] != ""),
                onTyping: (value) {
                  if (value.length == 0) {
                    verifyIdentityProvider.setFieldsValue("", 2);
                    verifyIdentityProvider.fields[1]["focusNode"]
                        .requestFocus();
                    checkIsAlreadyFill(verifyIdentityProvider);
                    return;
                  }
                  if (verifyIdentityProvider.fields[2]["value"].length >= 1) {
                    verifyIdentityProvider.fields[3]["focusNode"]
                        .requestFocus();
                    checkIsAlreadyFill(verifyIdentityProvider);
                  } else {
                    if (value.length == 1) {
                      verifyIdentityProvider.setFieldsValue(value, 2);
                      verifyIdentityProvider.fields[3]["focusNode"]
                          .requestFocus();
                      checkIsAlreadyFill(verifyIdentityProvider);
                    } else {
                      verifyIdentityProvider.setFieldsValue("", 2);
                      verifyIdentityProvider.fields[1]["focusNode"]
                          .requestFocus();
                      checkIsAlreadyFill(verifyIdentityProvider);
                    }
                  }
                },
              )),
              SizedBox(width: 8),
              Expanded(
                  child: FieldPIN(
                field: verifyIdentityProvider.fields[3],
                isAvalid: (verifyIdentityProvider.fields[3]["value"] != ""),
                onTyping: (value) {
                  if (value.length == 0) {
                    verifyIdentityProvider.setFieldsValue("", 3);
                    verifyIdentityProvider.fields[2]["focusNode"]
                        .requestFocus();
                    checkIsAlreadyFill(verifyIdentityProvider);
                    return;
                  }
                  if (verifyIdentityProvider.fields[3]["value"].length >= 1) {
                    verifyIdentityProvider.setPIN(verifyIdentityProvider.fields
                        .map((e) => e["value"])
                        .join(""));
                    FocusScope.of(context).unfocus();
                    checkIsAlreadyFill(verifyIdentityProvider);
                  } else {
                    if (value.length == 1) {
                      verifyIdentityProvider.setFieldsValue(value, 3);
                      verifyIdentityProvider.setPIN(verifyIdentityProvider
                          .fields
                          .map((e) => e["value"])
                          .join(""));
                      FocusScope.of(context).unfocus();
                      checkIsAlreadyFill(verifyIdentityProvider);
                    } else {
                      verifyIdentityProvider.setFieldsValue("", 3);
                      verifyIdentityProvider.fields[2]["focusNode"]
                          .requestFocus();
                      checkIsAlreadyFill(verifyIdentityProvider);
                    }
                  }
                },
              ))
            ],
          ),
        ),
      ],
    );
  }

  checkIsAlreadyFill(VerifyIdentityMD verifyIdentityProvider) {
    String pin = "";
    verifyIdentityProvider.fields.forEach((element) {
      pin += element["value"];
    });
    print(pin);
    verifyIdentityProvider.setPIN(pin);
  }

  @override
  Widget build(BuildContext context) {
    final verifyIdentityProvider =
        Provider.of<VerifyIdentityMD>(context, listen: true);
    final provider = Provider.of<AuthMD>(context);

    return Scaffold(
        backgroundColor: Tint.background,
        body: SafeArea(
            child: Template(
          onClose: () => provider.setCurrentPage(LoginPageProvider.pageName),
          currentPage: VerifyIdentityPageProvider.pageName,
          view: setupPage(verifyIdentityProvider, provider),
          actionTop: Container(),
        )),
        bottomNavigationBar: Column(mainAxisSize: MainAxisSize.min, children: [
          Container(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: (verifyIdentityProvider.isFormValid)
                      ? Tint.main
                      : Tint.disabled,
                  textStyle: const TextStyle(fontSize: 16),
                  padding: EdgeInsets.symmetric(vertical: 16)),
              onPressed: () => {
                if (verifyIdentityProvider.isFormValid)
                  provider.setCurrentPage(ResetPasswordPageProvider.pageName)
                else
                  null
              },
              child: Text(provider.content["buttons"][11]),
            ),
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(16),
            color: Colors.white,
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(children: [
                  TextSpan(
                    text: provider.content["buttons"][5] + "\n",
                    style: TextStyle(color: Tint.text),
                  ),
                  TextSpan(
                      text: provider.content["buttons"][7] + " ",
                      style: TextStyle(color: Tint.main)),
                ])),
            padding: EdgeInsets.only(bottom: 16),
            color: Colors.white,
          ),
        ]));
  }
}

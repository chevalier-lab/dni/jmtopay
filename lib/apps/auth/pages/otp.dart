// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:jmtopay/apps/auth/components/template.dart';
// import 'package:jmtopay/apps/auth/pages/login.dart';
// import 'package:jmtopay/components/custom_description.dart';
// import 'package:jmtopay/components/custom_field.dart';
// import 'package:jmtopay/components/custom_heading.dart';
// import 'package:jmtopay/helper/tint.dart';
// import 'package:jmtopay/models/Auth_md.dart';
// import 'package:jmtopay/models/auth/otp_md.dart';
// import 'package:provider/provider.dart';

// class OTPPageProvider {
//   static const String pageName = "/otp";

//   static Widget init(BuildContext context) {
//     return ChangeNotifierProvider<OTPMD>(
//         create: (_) => OTPMD(), child: OTPPage());
//   }
// }

// class OTPPage extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() => _OTPPage();
// }

// class _OTPPage extends State<OTPPage> {
//   Widget setupPage(OTPMD otpProvider, provider) {
//     return Column(
//       children: [
//         Heading(
//             label: provider.content["title"][OTPPageProvider.pageName]
//                 ["label"]),
//         SizedBox(height: 16),
//         Description(
//             label: provider.content["description"][OTPPageProvider.pageName]
//                 ["label"]),
//         SizedBox(height: 32),
//         Container(
//           padding: EdgeInsets.symmetric(horizontal: 16),
//           child: Row(
//             children: [
//               Expanded(
//                   child: FieldPIN(
//                 field: otpProvider.fields[0],
//                 isAvalid: (otpProvider.fields[0]["value"] != ""),
//                 onTyping: (value) {
//                   if (value.length == 0) {
//                     otpProvider.setFieldsValue("", 0);
//                     return;
//                   }
//                   if (otpProvider.fields[0]["value"].length >= 1) {
//                     otpProvider.fields[1]["focusNode"].requestFocus();
//                   } else {
//                     if (value.length == 1) {
//                       otpProvider.setFieldsValue(value, 0);
//                       otpProvider.fields[1]["focusNode"].requestFocus();
//                     } else {
//                       otpProvider.setFieldsValue("", 0);
//                     }
//                   }
//                 },
//               )),
//               SizedBox(width: 8),
//               Expanded(
//                   child: FieldPIN(
//                 field: otpProvider.fields[1],
//                 isAvalid: (otpProvider.fields[1]["value"] != ""),
//                 onTyping: (value) {
//                   if (value.length == 0) {
//                     otpProvider.setFieldsValue("", 1);
//                     otpProvider.fields[0]["focusNode"].requestFocus();
//                     return;
//                   }
//                   if (otpProvider.fields[1]["value"].length >= 1) {
//                     otpProvider.fields[2]["focusNode"].requestFocus();
//                   } else {
//                     if (value.length == 1) {
//                       otpProvider.setFieldsValue(value, 1);
//                       otpProvider.fields[2]["focusNode"].requestFocus();
//                     } else {
//                       otpProvider.setFieldsValue("", 2);
//                       otpProvider.fields[1]["focusNode"].requestFocus();
//                     }
//                   }
//                 },
//               )),
//               SizedBox(width: 8),
//               Expanded(
//                   child: FieldPIN(
//                 field: otpProvider.fields[2],
//                 isAvalid: (otpProvider.fields[2]["value"] != ""),
//                 onTyping: (value) {
//                   if (value.length == 0) {
//                     otpProvider.setFieldsValue("", 2);
//                     otpProvider.fields[1]["focusNode"].requestFocus();
//                     return;
//                   }
//                   if (otpProvider.fields[2]["value"].length >= 1) {
//                     otpProvider.fields[3]["focusNode"].requestFocus();
//                   } else {
//                     if (value.length == 1) {
//                       otpProvider.setFieldsValue(value, 2);
//                       otpProvider.fields[3]["focusNode"].requestFocus();
//                     } else {
//                       otpProvider.setFieldsValue("", 2);
//                       otpProvider.fields[1]["focusNode"].requestFocus();
//                     }
//                   }
//                 },
//               )),
//               SizedBox(width: 8),
//               Expanded(
//                   child: FieldPIN(
//                 field: otpProvider.fields[3],
//                 isAvalid: (otpProvider.fields[3]["value"] != ""),
//                 onTyping: (value) {
//                   if (value.length == 0) {
//                     otpProvider.setFieldsValue("", 3);
//                     otpProvider.fields[2]["focusNode"].requestFocus();
//                     return;
//                   }
//                   if (otpProvider.fields[3]["value"].length >= 1) {
//                     otpProvider.setPIN(
//                         otpProvider.fields.map((e) => e["value"]).join(""));
//                     FocusScope.of(context).unfocus();
//                   } else {
//                     if (value.length == 1) {
//                       otpProvider.setFieldsValue(value, 3);
//                       otpProvider.setPIN(
//                           otpProvider.fields.map((e) => e["value"]).join(""));
//                       FocusScope.of(context).unfocus();
//                     } else {
//                       otpProvider.setFieldsValue("", 3);
//                       otpProvider.fields[2]["focusNode"].requestFocus();
//                     }
//                   }
//                 },
//               ))
//             ],
//           ),
//         ),
//       ],
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     final otpProvider = Provider.of<OTPMD>(context, listen: true);
//     final provider = Provider.of<AuthMD>(context);

//     return Scaffold(
//         backgroundColor: Tint.background,
//         body: SafeArea(
//             child: Template(
//           onClose: () => provider.setCurrentPage(LoginPageProvider.pageName),
//           currentPage: OTPPageProvider.pageName,
//           view: setupPage(otpProvider, provider),
//           actionTop: Container(),
//         )),
//         bottomNavigationBar: Column(mainAxisSize: MainAxisSize.min, children: [
//           Container(
//             child: ElevatedButton(
//               style: ElevatedButton.styleFrom(
//                   primary:
//                       (otpProvider.isFormValid) ? Tint.main : Tint.disabled,
//                   textStyle: const TextStyle(fontSize: 16),
//                   padding: EdgeInsets.symmetric(vertical: 16)),
//               onPressed: () => {if (otpProvider.isFormValid) null else null},
//               child: Text(provider.content["buttons"][11]),
//             ),
//             width: MediaQuery.of(context).size.width,
//             padding: EdgeInsets.all(16),
//             color: Colors.white,
//           ),
//           Container(
//             width: MediaQuery.of(context).size.width,
//             child: RichText(
//                 textAlign: TextAlign.center,
//                 text: TextSpan(children: [
//                   TextSpan(
//                     text: provider.content["buttons"][5] + "\n",
//                     style: TextStyle(color: Tint.text),
//                   ),
//                   TextSpan(
//                       text: provider.content["buttons"][7] + " ",
//                       style: TextStyle(color: Tint.main)),
//                 ])),
//             padding: EdgeInsets.only(bottom: 16),
//             color: Colors.white,
//           ),
//         ]));
//   }
// }

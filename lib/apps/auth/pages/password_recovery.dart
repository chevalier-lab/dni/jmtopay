import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/auth/components/template.dart';
import 'package:jmtopay/apps/auth/pages/login.dart';
import 'package:jmtopay/apps/auth/pages/verify_identity.dart';
import 'package:jmtopay/components/custom_description.dart';
import 'package:jmtopay/components/custom_field.dart';
import 'package:jmtopay/components/custom_heading.dart';
import 'package:jmtopay/helper/tint.dart';
import 'package:jmtopay/models/Auth_md.dart';
import 'package:jmtopay/models/auth/password_recovery_md.dart';
import 'package:provider/provider.dart';

class PasswordRecoveryPageProvider {
  static const String pageName = "/password-recovery";

  static Widget init(BuildContext context) {
    return ChangeNotifierProvider<PasswordRecoveryMD>(
        create: (_) => PasswordRecoveryMD(), child: PasswordRecoveryPage());
  }
}

class PasswordRecoveryPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PasswordRecoveryPage();
}

class _PasswordRecoveryPage extends State<PasswordRecoveryPage> {
  Widget setupPage(
      PasswordRecoveryMD passwordRecoveryProvider, AuthMD provider) {
    return Column(
      children: [
        Heading(
            label: provider.content["title"]
                [PasswordRecoveryPageProvider.pageName]["label"]),
        SizedBox(height: 16),
        Description(
            label: provider.content["description"]
                [PasswordRecoveryPageProvider.pageName]["label"]),
        SizedBox(height: 32),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: FieldPassword(
            field: provider.content["fields"][0],
            isAvalid: passwordRecoveryProvider.checkIsPhoneNumberValid(),
            onTyping: (String value) =>
                passwordRecoveryProvider.setPhoneNumber(value),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final passwordRecoveryProvider =
        Provider.of<PasswordRecoveryMD>(context, listen: true);
    final provider = Provider.of<AuthMD>(context);

    return Scaffold(
      body: SafeArea(
        child: Template(
            onClose: () => provider.setCurrentPage(LoginPageProvider.pageName),
            currentPage: PasswordRecoveryPageProvider.pageName,
            view: setupPage(passwordRecoveryProvider, provider),
            actionTop: Container()),
      ),
      bottomNavigationBar: Container(
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: (passwordRecoveryProvider.isFormValid)
                  ? Tint.main
                  : Tint.disabled,
              textStyle: const TextStyle(fontSize: 16),
              padding: EdgeInsets.symmetric(vertical: 16)),
          onPressed: () => {
            if (passwordRecoveryProvider.isFormValid)
              provider.setCurrentPage(VerifyIdentityPageProvider.pageName)
            else
              null
          },
          child: Text(provider.content["buttons"][10]),
        ),
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(16),
        color: Colors.white,
      ),
    );
  }
}

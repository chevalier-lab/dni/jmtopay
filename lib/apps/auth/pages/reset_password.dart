import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/auth/components/template.dart';
import 'package:jmtopay/apps/auth/pages/login.dart';
import 'package:jmtopay/components/custom_description.dart';
import 'package:jmtopay/components/custom_field.dart';
import 'package:jmtopay/components/custom_heading.dart';
import 'package:jmtopay/helper/tint.dart';
import 'package:jmtopay/models/Auth_md.dart';
import 'package:jmtopay/models/auth/reset_password.dart';
import 'package:provider/provider.dart';

class ResetPasswordPageProvider {
  static const String pageName = "/reset-password";

  static Widget init(BuildContext context) {
    return ChangeNotifierProvider<ResetPasswordMD>(
        create: (_) => ResetPasswordMD(), child: ResetPasswordPage());
  }
}

class ResetPasswordPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ResetPasswordPage();
}

class _ResetPasswordPage extends State<ResetPasswordPage> {
  Widget setupPage(ResetPasswordMD resetPasswordProvider, AuthMD provider) {
    return Column(
      children: [
        Heading(
            label: provider.content["title"][ResetPasswordPageProvider.pageName]
                ["label"]),
        SizedBox(height: 16),
        Description(
            label: provider.content["description"]
                [ResetPasswordPageProvider.pageName]["label"]),
        SizedBox(height: 32),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: FieldPassword(
            field: provider.content["fields"][3],
            isAvalid: resetPasswordProvider.checkIsNewPasswordValid(),
            onTyping: (String value) =>
                resetPasswordProvider.setNewPassword(value),
          ),
        ),
        SizedBox(height: 16),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: FieldPassword(
            field: provider.content["fields"][4],
            isAvalid: resetPasswordProvider.checkIsConfirmPasswordValid(),
            onTyping: (String value) =>
                resetPasswordProvider.setConfirmPassword(value),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final resetPasswordProvider =
        Provider.of<ResetPasswordMD>(context, listen: true);
    final provider = Provider.of<AuthMD>(context);

    return Scaffold(
      body: SafeArea(
        child: Template(
            onClose: () => provider.setCurrentPage(LoginPageProvider.pageName),
            currentPage: ResetPasswordPageProvider.pageName,
            view: setupPage(resetPasswordProvider, provider),
            actionTop: Container()),
      ),
      bottomNavigationBar: Container(
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: (resetPasswordProvider.isFormValid)
                  ? Tint.main
                  : Tint.disabled,
              textStyle: const TextStyle(fontSize: 16),
              padding: EdgeInsets.symmetric(vertical: 16)),
          onPressed: () => {
            if (resetPasswordProvider.isFormValid)
              provider.setCurrentPage(LoginPageProvider.pageName)
            else
              null
          },
          child: Text(provider.content["buttons"][0]),
        ),
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(16),
        color: Colors.white,
      ),
    );
  }
}

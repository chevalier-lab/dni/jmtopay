import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/auth/components/template.dart';
import 'package:jmtopay/apps/auth/injections/registration.dart';
import 'package:jmtopay/apps/auth/pages/login.dart';
import 'package:jmtopay/apps/auth/popups/otp.dart';
import 'package:jmtopay/apps/home/provider.dart';
import 'package:jmtopay/components/custom_description.dart';
import 'package:jmtopay/components/custom_field.dart';
import 'package:jmtopay/components/custom_heading.dart';
import 'package:jmtopay/helper/sheet.dart';
import 'package:jmtopay/helper/tint.dart';
import 'package:jmtopay/models/Auth_md.dart';
import 'package:jmtopay/models/auth/registration_md.dart';
import 'package:jmtopay/utils/device.dart';
import 'package:jmtopay/utils/location.dart';
import 'package:provider/provider.dart';

class RegistrationPageProvider {
  static const String pageName = "/registration";

  static Widget init(BuildContext context) {
    return ChangeNotifierProvider<RegistrationMD>(
        create: (_) => RegistrationMD(), child: RegistrationPage());
  }
}

class RegistrationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RegistrationPage();
}

class _RegistrationPage extends State<RegistrationPage> {
  final injections = RegistrationInjections();
  dynamic latLng = {"lat": "0.0000000", "lng": "0.0000000"};
  String deviceID = "";
  final location = Location();
  final device = Device();

  @override
  void initState() {
    super.initState();

    setupDeviceService();
    setupLocationServices();
    injections.initialize(context);
  }

  // Setup Device Service
  void setupDeviceService() async {
    String tempDeviceID = await device.getID();
    setState(() {
      deviceID = tempDeviceID;
    });
    print("DEVICE_ID $deviceID");
  }

  // Setup Location Services
  void setupLocationServices() async {
    location.getCurrentLocation((data) {
      setState(() {
        this.latLng = data;
      });
      print("LATLONG $data");
    });
  }

  // Open Detail Product
  Future openOTPPopups(
      BuildContext context,
      AuthMD provider,
      String fullName,
      String username,
      String email,
      String phoneNumber,
      String password) async {
    dynamic response =
        await Navigator.of(context).push(new MaterialPageRoute<dynamic>(
            builder: (BuildContext context) {
              return OTPPage(provider: provider, phoneNumber: phoneNumber);
            },
            fullscreenDialog: true));
    if (response != null) {
      if (response["status"]) {
        doRegistration(fullName, username, email, phoneNumber, password);
      } else
        error(context, response["message"]);
    }
  }

  // Do Registration
  void doRegistration(String fullName, String username, String email,
      String phoneNumber, String password) {
    this
        .injections
        .doRegistration(fullName, username, email, phoneNumber, password, () {
      Navigator.of(context)
          .pushNamedAndRemoveUntil(HomeProvider.routeName, (route) => false);
    });
  }

  Widget setupPage(RegistrationMD registrationProvider, AuthMD provider) {
    return Column(
      children: [
        Heading(
            label: provider.content["title"][RegistrationPageProvider.pageName]
                ["label"]),
        SizedBox(height: 16),
        Description(
            label: provider.content["description"]
                [RegistrationPageProvider.pageName]["label"]),
        SizedBox(height: 32),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Field(
            field: provider.content["fields"][2],
            isAvalid: registrationProvider.checkIsFullNameValid(),
            onTyping: (String value) => registrationProvider.setFullName(value),
          ),
        ),
        SizedBox(height: 16),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Field(
            field: provider.content["fields"][5],
            isAvalid: registrationProvider.checkIsUsernameValid(),
            onTyping: (String value) => registrationProvider.setUsername(value),
          ),
        ),
        SizedBox(height: 16),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Field(
            field: provider.content["fields"][6],
            isAvalid: registrationProvider.checkIsEmailValid(),
            onTyping: (String value) => registrationProvider.setEmail(value),
          ),
        ),
        SizedBox(height: 16),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Field(
            field: provider.content["fields"][0],
            isAvalid: registrationProvider.checkIsPhoneNumberValid(),
            onTyping: (String value) =>
                registrationProvider.setPhoneNumber(value),
          ),
        ),
        SizedBox(height: 16),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: FieldPassword(
            field: provider.content["fields"][1],
            isAvalid: registrationProvider.checkIsPasswordValid(),
            onTyping: (String value) => registrationProvider.setPassword(value),
          ),
        ),
        SizedBox(height: 16),
        GestureDetector(
          onTap: () {
            if (registrationProvider.checkIsAggreement())
              registrationProvider.setAgreement(false);
            else
              registrationProvider.setAgreement(true);
          },
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Row(
              children: [
                registrationProvider.checkIsAggreement()
                    ? Icon(Icons.check_box, color: Tint.main)
                    : Icon(Icons.check_box_outline_blank, color: Tint.field),
                SizedBox(width: 8),
                Expanded(
                    child: RichText(
                        text: TextSpan(children: [
                  TextSpan(
                      text: provider.content["buttons"][6] + "\n",
                      style: TextStyle(color: Tint.text)),
                  TextSpan(
                      text: provider.content["buttons"][7],
                      style: TextStyle(color: Tint.main)),
                ])))
              ],
            ),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final registrationProvider =
        Provider.of<RegistrationMD>(context, listen: true);
    final provider = Provider.of<AuthMD>(context);

    return Scaffold(
      backgroundColor: Tint.background,
      body: SafeArea(
          child: Template(
        onClose: () => provider.setCurrentPage(LoginPageProvider.pageName),
        currentPage: RegistrationPageProvider.pageName,
        view: setupPage(registrationProvider, provider),
        actionTop: GestureDetector(
          child: RichText(
              text: TextSpan(children: [
            TextSpan(
                text: provider.content["buttons"][9] + " ",
                style: TextStyle(color: Tint.text)),
            TextSpan(
                text: provider.content["buttons"][0] + " ",
                style: TextStyle(color: Tint.main)),
          ])),
          onTap: () => provider.setCurrentPage(LoginPageProvider.pageName),
        ),
      )),
      bottomNavigationBar: Container(
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: (registrationProvider.isFormValid)
                  ? Tint.main
                  : Tint.disabled,
              textStyle: const TextStyle(fontSize: 16),
              padding: EdgeInsets.symmetric(vertical: 16)),
          onPressed: () => {
            if (registrationProvider.isFormValid)
              openOTPPopups(
                  context,
                  provider,
                  registrationProvider.fieldFullName,
                  registrationProvider.fieldUsername,
                  registrationProvider.fieldEmail,
                  registrationProvider.fieldPhoneNumber,
                  registrationProvider.fieldPassword)
            else
              null
          },
          child: Text(provider.content["buttons"][8]),
        ),
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(16),
        color: Colors.white,
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/auth/components/template.dart';
import 'package:jmtopay/apps/auth/injections/login.dart';
import 'package:jmtopay/apps/auth/pages/password_recovery.dart';
import 'package:jmtopay/apps/auth/pages/registration.dart';
import 'package:jmtopay/apps/auth/popups/otp.dart';
import 'package:jmtopay/apps/home/provider.dart';
import 'package:jmtopay/components/custom_description.dart';
import 'package:jmtopay/components/custom_field.dart';
import 'package:jmtopay/components/custom_heading.dart';
import 'package:jmtopay/helper/cache.dart';
import 'package:jmtopay/helper/sheet.dart';
import 'package:jmtopay/helper/tint.dart';
import 'package:jmtopay/models/Auth_md.dart';
import 'package:jmtopay/models/auth/login_md.dart';
import 'package:jmtopay/utils/device.dart';
import 'package:jmtopay/utils/location.dart';
import 'package:provider/provider.dart';

class LoginPageProvider {
  static const String pageName = "/login";

  static Widget init(BuildContext context) {
    return ChangeNotifierProvider<LoginMD>(
        create: (_) => LoginMD(), child: LoginPage());
  }
}

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginPage();
}

class _LoginPage extends State<LoginPage> {
  final injection = LoginInjections();
  dynamic latLng = {"lat": "0.0000000", "lng": "0.0000000"};
  String deviceID = "";
  final location = Location();
  final device = Device();

  @override
  void initState() {
    super.initState();

    setupDeviceService();
    setupLocationServices();
    injection.initialize(context);
  }

  // Setup Device Service
  void setupDeviceService() async {
    String tempDeviceID = await device.getID();
    setState(() {
      deviceID = tempDeviceID;
    });
    print("DEVICE_ID $deviceID");
  }

  // Setup Location Services
  void setupLocationServices() async {
    location.getCurrentLocation((data) {
      setState(() {
        this.latLng = data;
      });
      print("LATLONG $data");
    });
  }

  // Open Detail Product
  Future openOTPPopups(BuildContext context, AuthMD provider,
      String phoneNumber, String password, dynamic auth) async {
    dynamic response =
        await Navigator.of(context).push(new MaterialPageRoute<dynamic>(
            builder: (BuildContext context) {
              return OTPPage(provider: provider, phoneNumber: phoneNumber);
            },
            fullscreenDialog: true));
    if (response != null) {
      if (response["status"]) {
        setAuth(auth);
        Navigator.of(context).pushReplacementNamed(HomeProvider.routeName);
      } else
        error(context, response["message"]);
    }
  }

  // Do Login
  void doLogin(AuthMD provider, String user, String password) {
    injection.doLogin(user, password, (data) {
      openOTPPopups(context, provider, user, password, data);
    });
  }

  // Setup Page
  Widget setupPage(loginProvider, provider) {
    return Column(
      children: [
        Heading(
            label: provider.content["title"][LoginPageProvider.pageName]
                ["label"]),
        SizedBox(height: 16),
        Description(
            label: provider.content["description"][LoginPageProvider.pageName]
                ["label"]),
        SizedBox(height: 32),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Field(
            field: provider.content["fields"][0],
            isAvalid: loginProvider.checkIsPhoneNumberValid(),
            onTyping: (String value) => loginProvider.setPhoneNumber(value),
          ),
        ),
        SizedBox(height: 16),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: FieldPassword(
            field: provider.content["fields"][1],
            isAvalid: loginProvider.checkIsPasswordValid(),
            onTyping: (String value) => loginProvider.setPassword(value),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final loginProvider = Provider.of<LoginMD>(context, listen: true);
    final provider = Provider.of<AuthMD>(context);

    return Scaffold(
      backgroundColor: Tint.background,
      body: SafeArea(
          child: Template(
        onClose: () => provider.setCurrentPage(LoginPageProvider.pageName),
        currentPage: LoginPageProvider.pageName,
        view: setupPage(loginProvider, provider),
        actionTop: GestureDetector(
          child: RichText(
              text: TextSpan(children: [
            TextSpan(
                text: provider.content["buttons"][2] + " ",
                style: TextStyle(color: Tint.text)),
          ])),
          onTap: () =>
              provider.setCurrentPage(PasswordRecoveryPageProvider.pageName),
        ),
      )),
      bottomNavigationBar: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary:
                      (loginProvider.isFormValid) ? Tint.main : Tint.disabled,
                  textStyle: const TextStyle(fontSize: 16),
                  padding: EdgeInsets.symmetric(vertical: 16)),
              onPressed: () => {
                if (loginProvider.isFormValid)
                  {
                    doLogin(provider, loginProvider.fieldPhoneNumber,
                        loginProvider.fieldPassword)
                  }
                else
                  null
              },
              child: Text(provider.content["buttons"][0]),
            ),
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 8),
            color: Colors.white,
          ),
          Container(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Tint.light,
                  textStyle: const TextStyle(fontSize: 16),
                  padding: EdgeInsets.symmetric(vertical: 16)),
              onPressed: () =>
                  provider.setCurrentPage(RegistrationPageProvider.pageName),
              child: Text(
                provider.content["buttons"][1],
                style: TextStyle(color: Colors.black),
              ),
            ),
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 16),
            color: Colors.white,
          )
        ],
      ),
    );
  }
}

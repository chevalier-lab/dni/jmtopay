import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:jmtopay/api/auth/registration.dart';
import 'package:jmtopay/helper/cache.dart';
import 'package:jmtopay/helper/global.dart';
import 'package:jmtopay/helper/request.dart';
import 'package:jmtopay/helper/sheet.dart';
import 'package:jmtopay/helper/url.dart';
import 'package:jmtopay/utils/component.dart';

class RegistrationInjections {
  Dio dio;
  RequestModel requestModel;
  RegistrationController controller;
  BuildContext context;

  // Setup Initialize
  void initialize(BuildContext context) {
    this.context = context;
    this.dio = models.initial(BaseURLAPI.uri);
    this.requestModel = RequestModel(dio: dio);

    this.controller = RegistrationController(
        context: context, requestModel: this.requestModel);
  }

  // Do Registration
  void doRegistration(String fullName, String username, String email,
      String phoneNumber, String password, VoidCallback onSuccess) {
    loader.build(context);
    controller.index({
      RequestKey.username: username,
      RequestKey.cmName: fullName,
      RequestKey.email: email,
      RequestKey.phone: phoneNumber,
      RequestKey.password: password
    }, (data) {
      Navigator.of(context).pop();
      setAuth(data);
      onSuccess();
    }, (message) {
      Navigator.of(context).pop();
      checkConnection((status) {
        if (status != "Connect")
          error(context, "Tidak terhubung ke internet.");
        else
          error(context, message);
      });
    });
  }
}

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:jmtopay/api/auth/login.dart';
import 'package:jmtopay/helper/global.dart';
import 'package:jmtopay/helper/request.dart';
import 'package:jmtopay/helper/sheet.dart';
import 'package:jmtopay/helper/url.dart';
import 'package:jmtopay/utils/component.dart';

class LoginInjections {
  Dio dio;
  RequestModel requestModel;
  LoginController controller;
  BuildContext context;

  // Setup Initialize
  void initialize(BuildContext context) {
    this.context = context;
    this.dio = models.initial(BaseURLAPI.uri);
    this.requestModel = RequestModel(dio: dio);

    this.controller =
        LoginController(context: context, requestModel: this.requestModel);
  }

  // Do Login
  void doLogin(String user, String password, Function(dynamic) onSuccess) {
    loader.build(context);
    controller.index({RequestKey.user: user, RequestKey.password: password},
        (data) {
      Navigator.of(context).pop();
      onSuccess(data);
    }, (message) {
      Navigator.of(context).pop();
      checkConnection((status) {
        if (status != "Connect")
          error(context, "Tidak terhubung ke internet.");
        else
          error(context, message);
      });
    });
  }
}

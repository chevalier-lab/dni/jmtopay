import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/apps/auth/pages/login.dart';
import 'package:jmtopay/apps/auth/pages/password_recovery.dart';
import 'package:jmtopay/apps/auth/pages/registration.dart';
import 'package:jmtopay/apps/auth/pages/reset_password.dart';
import 'package:jmtopay/apps/auth/pages/verify_identity.dart';
import 'package:jmtopay/models/Auth_md.dart';
import 'package:provider/provider.dart';

class AuthView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AuthView();
}

class _AuthView extends State<AuthView> {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<AuthMD>(context);

    switch (provider.currentPage) {
      case LoginPageProvider.pageName:
        return LoginPageProvider.init(context);
        break;
      case RegistrationPageProvider.pageName:
        return RegistrationPageProvider.init(context);
        break;
      case PasswordRecoveryPageProvider.pageName:
        return PasswordRecoveryPageProvider.init(context);
        break;
      case VerifyIdentityPageProvider.pageName:
        return VerifyIdentityPageProvider.init(context);
        break;
      case ResetPasswordPageProvider.pageName:
        return ResetPasswordPageProvider.init(context);
        break;
      default:
        return LoginPageProvider.init(context);
        break;
    }
  }
}

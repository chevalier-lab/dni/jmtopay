import 'package:flutter/cupertino.dart';
import 'package:jmtopay/helper/request.dart';
import 'package:jmtopay/helper/url.dart';

class PaymentController {
  final BuildContext context;
  final RequestModel requestModel;

  PaymentController({
    @required this.context,
    @required this.requestModel,
  });

  // Get Payments
  void lists(Function(dynamic) onSuccess, Function(String) onError) async {
    this.requestModel.getData(context, BaseURLAPI.payments, (res) {
      // context,
      // BaseURLAPI.transactions + "6", (res) {
      if (res[RequestKey.code] == 200) {
        onSuccess(res[RequestKey.data]);
      } else
        onError(res[RequestKey.message]);
    }, (err) => onError(err));
  }
}

import 'package:flutter/cupertino.dart';
import 'package:jmtopay/helper/cache.dart';
import 'package:jmtopay/helper/request.dart';
import 'package:jmtopay/helper/url.dart';

class TransactionController {
  final BuildContext context;
  final RequestModel requestModel;

  TransactionController({
    @required this.context,
    @required this.requestModel,
  });

  // Get Transactions
  void lists(Function(dynamic) onSuccess, Function(String) onError) async {
    final auth = await getAuth();
    this.requestModel.getData(
        context, BaseURLAPI.transactions + auth[RequestKey.cmID], (res) {
      // context,
      // BaseURLAPI.transactions + "4", (res) {
      if (res[RequestKey.code] == 200) {
        onSuccess(res[RequestKey.data]);
      } else
        onError(res[RequestKey.message]);
    }, (err) => onError(err));
  }

  // Get Detail Transactions
  void detail(var transactionID, Function(dynamic) onSuccess,
      Function(String) onError) async {
    this
        .requestModel
        .getData(context, BaseURLAPI.transactionDetail + transactionID, (res) {
      // context,
      // BaseURLAPI.transactions + "4", (res) {
      if (res[RequestKey.code] == 200) {
        onSuccess(res[RequestKey.data][0]);
      } else
        onError(res[RequestKey.message]);
    }, (err) => onError(err));
  }

  // Do Order
  void order(
      var req, Function(String) onSuccess, Function(String) onError) async {
    final auth = await getAuth();
    this
        .requestModel
        .rawData(context, BaseURLAPI.order + auth[RequestKey.cmID], req, (res) {
      // .rawData(context, BaseURLAPI.order + "4", req, (res) {
      if (res[RequestKey.code] == 200) {
        onSuccess(res[RequestKey.message]);
      } else
        onError(res[RequestKey.message]);
    }, (err) => onError(err));
  }

  // Do Order Midtrans
  void orderMidtrans(
      var req, Function(dynamic) onSuccess, Function(String) onError) async {
    final auth = await getAuth();
    req[RequestKey.cmID] = auth[RequestKey.cmID];
    // req[RequestKey.cmID] = "4";
    this.requestModel.formData(context, BaseURLAPI.orderMidtrans, req, (res) {
      if (res[RequestKey.code] == 200) {
        onSuccess(res);
      } else
        onError(res[RequestKey.message]);
    }, (err) => onError(err));
  }

  // Do Order Link Aja
  void orderLinkAja(
      var req, Function(String) onSuccess, Function(String) onError) async {
    final auth = await getAuth();
    req[RequestKey.cmID] = auth[RequestKey.cmID];
    // req[RequestKey.cmID] = "4";

    print(req);

    this.requestModel.rawData(context, BaseURLAPI.orderLinkAja, req, (res) {
      if (res[RequestKey.code] == 200) {
        onSuccess(res[RequestKey.message]);
      } else
        onError(res[RequestKey.message]);
    }, (err) => onError(err));
  }
}

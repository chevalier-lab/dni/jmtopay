import 'package:flutter/cupertino.dart';
import 'package:jmtopay/helper/request.dart';
import 'package:jmtopay/helper/url.dart';

class RegistrationController {
  final BuildContext context;
  final RequestModel requestModel;

  RegistrationController({
    @required this.context,
    @required this.requestModel,
  });

  // Do Registration
  void index(var req, Function(dynamic) onSuccess, Function(String) onError) {
    this.requestModel.rawData(context, BaseURLAPI.register, req, (res) {
      if (res[RequestKey.code] == 200) {
        onSuccess(res[RequestKey.data]);
      } else
        onError(res[RequestKey.message]);
    }, (err) => onError(err));
  }
}

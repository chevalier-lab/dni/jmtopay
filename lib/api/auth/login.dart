import 'package:flutter/cupertino.dart';
import 'package:jmtopay/helper/request.dart';
import 'package:jmtopay/helper/url.dart';

class LoginController {
  final BuildContext context;
  final RequestModel requestModel;

  LoginController({
    @required this.context,
    @required this.requestModel,
  });

  // Do Login
  void index(var req, Function(dynamic) onSuccess, Function(String) onError) {
    this.requestModel.rawData(context, BaseURLAPI.login, req, (res) {
      if (res[RequestKey.code] == 200) {
        onSuccess(res[RequestKey.data]);
      } else
        onError(res[RequestKey.message]);
    }, (err) => onError(err));
  }
}

import 'package:flutter/cupertino.dart';
import 'package:jmtopay/helper/request.dart';
import 'package:jmtopay/helper/url.dart';

class ProductController {
  final BuildContext context;
  final RequestModel requestModel;

  ProductController({
    @required this.context,
    @required this.requestModel,
  });

  // Get Product
  void lists(Function(dynamic) onSuccess, Function(String) onError) {
    this.requestModel.getData(context, BaseURLAPI.products, (res) {
      if (res[RequestKey.code] == 200) {
        onSuccess(res[RequestKey.data]);
      } else
        onError(res[RequestKey.message]);
    }, (err) => onError(err));
  }
}

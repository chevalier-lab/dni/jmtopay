import 'package:flutter/cupertino.dart';
import 'package:jmtopay/helper/request.dart';

class LinkAjaController {
  final BuildContext context;
  final RequestModel requestModel;

  LinkAjaController({
    @required this.context,
    @required this.requestModel,
  });

  // Do Register Binding Linkaja
  void openFrame(
      var req, Function(dynamic) onSuccess, Function(String) onError) async {
    this.requestModel.formUrlEncodedRaw(
        context, "https://webdev.linkaja.com/dd/debit/enable", req, (res) {
      onSuccess(res);
    }, (err) => onError(err));
  }
}

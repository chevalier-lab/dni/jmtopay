import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:jmtopay/helper/cache.dart';
import 'package:jmtopay/helper/request.dart';
import 'package:jmtopay/helper/url.dart';

class BindingsController {
  final BuildContext context;
  final RequestModel requestModel;

  BindingsController({
    @required this.context,
    @required this.requestModel,
  });

  // Do Register Binding
  void register(
      var req, Function(String) onSuccess, Function(String) onError) async {
    final auth = await getAuth();
    // req[RequestKey.cmID] = "1";
    req[RequestKey.cmID] = auth[RequestKey.cmID];
    req[RequestKey.bindingStatus] = "NEW";
    req[RequestKey.registrationToken] = "";
    print(jsonEncode(req));
    this.requestModel.formData(context, BaseURLAPI.binding, req, (res) {
      if (res[RequestKey.code] == 200) {
        onSuccess(res[RequestKey.message]);
      } else
        onError(res[RequestKey.message]);
    }, (err) => onError(err));
  }

  // Do Register Binding Linkaja
  void registerLinkAja(
      var req, Function(dynamic) onSuccess, Function(String) onError) async {
    final auth = await getAuth();
    // req[RequestKey.cmID] = "1";
    req[RequestKey.cmID] = auth[RequestKey.cmID];
    req[RequestKey.bindingStatus] = "NEW";
    req[RequestKey.registrationToken] = "";
    print(jsonEncode(req));
    this.requestModel.formData(context, BaseURLAPI.bindingLinkAja, req, (res) {
      if (res[RequestKey.code] == 200) {
        onSuccess(res["data"]);
      } else
        onError(res[RequestKey.message]);
    }, (err) => onError(err));
  }

  // Do Verify Binding
  void verify(
      var req, Function(String) onSuccess, Function(String) onError) async {
    final auth = await getAuth();
    // req[RequestKey.cmID] = "1";
    req[RequestKey.cmID] = auth[RequestKey.cmID];
    req[RequestKey.merchantID] = "3";
    print(jsonEncode(req));
    this.requestModel.formData(context, BaseURLAPI.bindingPasscode, req, (res) {
      if (res[RequestKey.code] == 200) {
        onSuccess(res[RequestKey.message]);
      } else
        onError(res[RequestKey.message]);
    }, (err) => onError(err));
  }

  // Check Binding
  void check(
      Function(List<dynamic>) onSuccess, Function(String) onError) async {
    final auth = await getAuth();
    this.requestModel.getData(
        context, BaseURLAPI.bindingCheck + auth[RequestKey.cmID], (res) {
      // context,
      // BaseURLAPI.bindingCheck + "4", (res) {
      if (res[RequestKey.code] == 200) {
        onSuccess(res[RequestKey.data]);
      } else
        onError(res[RequestKey.message]);
    }, (err) => onError(err));
  }

  // Do UnBinding
  void unBinding(
      var req, Function(String) onSuccess, Function(String) onError) async {
    final auth = await getAuth();
    // req[RequestKey.cmID] = "1";
    req[RequestKey.cmID] = auth[RequestKey.cmID];
    print(jsonEncode(req));
    this.requestModel.formData(context, BaseURLAPI.unBinding, req, (res) {
      if (res[RequestKey.code] == 200) {
        onSuccess(res[RequestKey.message]);
      } else
        onError(res[RequestKey.message]);
    }, (err) => onError(err));
  }

  // Do UnBinding Link Aja
  void unBindingLinkAja(var bindingID, Function(String) onSuccess,
      Function(String) onError) async {
    this.requestModel.getData(context, BaseURLAPI.unBindingLinkAja + bindingID,
        (res) {
      if (res[RequestKey.code] == 200) {
        onSuccess(res[RequestKey.message]);
      } else
        onError(res[RequestKey.message]);
    }, (err) => onError(err));
  }
}

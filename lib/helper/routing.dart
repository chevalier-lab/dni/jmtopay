import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jmtopay/helper/global.dart';
import 'package:jmtopay/helper/str.dart';
import 'package:jmtopay/helper/tint.dart';
import 'package:jmtopay/utils/route_bridge.dart';

class Routing extends StatefulWidget {
  @override
  State createState() => Application();
}

class Application extends State<Routing> {
  @override
  Widget build(BuildContext context) {
    // SystemChrome.setSystemUIOverlayStyle(
    //     SystemUiOverlayStyle(statusBarColor: Tint.white));

    final apps = MaterialApp(
      title: STR.title,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: createMaterialColor(Tint.main)),
      initialRoute: initialRoute,
      routes: routeBridge,
    );

    return apps;
  }
}

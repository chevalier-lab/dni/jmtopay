import 'package:flutter/cupertino.dart';

class Tint {
  static final Color main = Color(0xFF00875A);
  static final Color disabled = Color(0xFFDFE1E5);
  static final Color text = Color(0xFF979797);
  static final Color label = Color(0xFFC1C7D0);
  static final Color light = Color(0xFFF4F5F7);
  static final Color hint = Color(0xFF7A869A);
  static final Color white = Color(0xFFFFFFFF);
  static final Color black = Color(0xFF000000);
  static final Color heading = Color(0xFF172B4D);
  static final Color field = Color(0xFF42526E);
  static final Color success = Color(0xFF00875A);
  static final Color danger = Color(0xFFDE350B);
  static final Color background = Color(0xFFE2FFEE);
  static final Color warningPale = Color(0xFFFFFAE5);
  static final Color warning = Color(0xFFFF991F);
  static final Color purple = Color(0xFF5243AA);
  static final Color blue = Color(0xFF0052CC);
}

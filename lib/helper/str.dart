class STR {
  static String title = "JMTO";

  static String bankAccount = "Bank Account";
  static String internationCard = "Internation Card";

  static String more = "More";
  static String services = "Services";
  static String general = "General";
  static String newNotification = "New Notification";
  static String earlierNotification = "Earlier Notification";
  static String historyTransaction = "Riwayat Perjalanan";
  static String products = "Products";
  static String detailTransaction = "Detail Transaksi";
  static String detailProduct = "Detail Product";

  static final String loadingMessage = "Sedang memproses, mohon menunggu...";
}

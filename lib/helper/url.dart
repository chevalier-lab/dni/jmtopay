class BaseURLAPI {
  static final String uri = "http://10.1.12.29:8080";
  // static final String uri = "http://jmpay.my.id/public/api/index.php";
  static final String uriMidtrans = "http://10.1.12.29:8081/api/v1";
  // static final String uriMidtrans = "http://midtrans.jmpay.my.id/public/api/v1";
  // static final String uri = "http://jmto.livedemo-dni.com/public/api/index.php";
  // Login
  static final String login = uri + "/customer_merchant/login/index";
  // Registration
  static final String register = uri + "/customer_merchant/registration/index";
  // Forgot Password
  static final String forgotPassword = uri + "forgot-password";
  // Products
  static final String products = uri + "/products/lists/index";
  static final String payments = uri + "/payments/lists/index";
  static final String transactions = uri + "/transactions/lists/index/";
  static final String transactionDetail = uri + "/transactions/detail/index/";
  static final String order = uri + "/transactions/order/index/";
  static final String orderMidtrans = uriMidtrans + "/bayar";
  static final String orderLinkAja = uri + "/transactions/order/linkaja";
  // Card
  static final String binding = uri + "/bindings/registration";
  static final String bindingLinkAja = uri + "/bindings/registration/linkaja";
  static final String bindingCheck = uri + "/bindings/registration/check_cm/";
  static final String bindingPasscode = uri + "/bindings/passcode";
  static final String unBinding = uri + "/bindings/unbinding/index";
  static final String unBindingLinkAja =
      uri + "/bindings/registration/linkaja_unbinding/";
}

class RequestKey {
  static final String code = "code";
  static final String message = "message";
  static final String data = "data";
  static final String cmName = "cm_name";
  static final String username = "username";
  static final String user = "user";
  static final String password = "password";
  static final String passcode = "passcode";
  static final String email = "email";
  static final String phone = "phone";
  static final String phoneNumber = "phone_number";
  static final String cmID = "cm_id";
  static final String merchantID = "merchant_id";
  static final String createdAt = "created_at";
  static final String updatedAt = "updated_at";
  static final String productID = "product_id";
  static final String bindingID = "binding_id";
  static final String productName = "product_name";
  static final String productCategory = "product_category";
  static final String amount = "amount";
  // static final String payment_id = "payment_id";
  static final String unit = "unit";
  static final String categoryName = "category_name";
  static final String merchantName = "merchant_name";
  static final String paymentName = "payment_name";
  static final String transactionNo = "transaction_no";
  static final String merchantAddress = "merchant_address";
  static final String desc = "desc";
  static final String deviceID = "device_id";
  static final String latitude = "latitude";
  static final String longitude = "longitude";
  static final String bindingStatus = "binding_status";
  static final String registrationToken = "registration_token";
  static final String cardPan = "card_pan";
  static final String expDate = "exp_date";
  static final String transactionID = "transaction_id";
}

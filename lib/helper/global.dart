import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:jmtopay/utils/model.dart';
import 'package:url_launcher/url_launcher.dart';

// Create Material Color
MaterialColor createMaterialColor(Color color) {
  List strengths = <double>[.05];
  Map swatch = <int, Color>{};
  final int r = color.red, g = color.green, b = color.blue;

  for (int i = 1; i < 10; i++) {
    strengths.add(0.1 * i);
  }
  strengths.forEach((strength) {
    final double ds = 0.5 - strength;
    swatch[(strength * 1000).round()] = Color.fromRGBO(
      r + ((ds < 0 ? r : (255 - r)) * ds).round(),
      g + ((ds < 0 ? g : (255 - g)) * ds).round(),
      b + ((ds < 0 ? b : (255 - b)) * ds).round(),
      1,
    );
  });
  return MaterialColor(color.value, swatch);
}

// Initial Models
final Models models = Models();

// Launch URL
void launchURL(_url) async =>
    await canLaunch(_url) ? await launch(_url) : throw 'Could not launch $_url';

// Format Currency
final formatCurrency = new NumberFormat.simpleCurrency(locale: 'id_ID');

// Check Connectivity
void checkConnection(Function(String) status) async {
  try {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      status("Connect");
    } else if (connectivityResult == ConnectivityResult.wifi) {
      status("Connect");
    } else
      status("Not Connect");
  } on PlatformException catch (e) {
    status("Not Connect");
    print(e.toString());
  }
}

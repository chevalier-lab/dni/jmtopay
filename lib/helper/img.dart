class IMG {
  static final String baseURL = "assets/";
  static final String logoHorizontal = baseURL + "logo/logo-horizontal.png";
  static final String logo = baseURL + "logo/logo.png";

  static final String welcomeSlider1 =
      baseURL + "welcome-slider/welcome-slider-1.png";
  static final String welcomeSlider2 =
      baseURL + "welcome-slider/welcome-slider-2.png";
  static final String welcomeSlider3 =
      baseURL + "welcome-slider/welcome-slider-3.png";

  static final String barclays = baseURL + "bank/barclays.png";
  static final String bca = baseURL + "bank/bca.png";
  static final String bni = baseURL + "bank/bni.png";
  static final String bri = baseURL + "bank/bri.png";
  static final String cimbiniaga = baseURL + "bank/cimbiniaga.png";
  static final String dki = baseURL + "bank/dki.png";
  static final String himbara = baseURL + "bank/himbara.png";
  static final String mandiri = baseURL + "bank/mandiri.png";
  static final String masterCard = baseURL + "bank/master-card.png";
  static final String payoneer = baseURL + "bank/payoneer.png";
  static final String paypal = baseURL + "bank/paypal.png";
  static final String visa = baseURL + "bank/visa.png";
  static final String linkaja = baseURL + "bank/linkaja.png";
  static final String gopay = baseURL + "bank/gopay.png";

  static final String more = baseURL + "services/more.png";
  static final String info = baseURL + "services/info.png";
  static final String cctv = baseURL + "services/cctv.png";
  static final String mobil = baseURL + "services/mobil.png";
  static final String navigation = baseURL + "services/navigation.png";
  static final String restArea = baseURL + "services/rest-area.png";
  static final String spbu = baseURL + "services/spbu.png";
  static final String topup = baseURL + "services/topup.png";

  static final String car = baseURL + "global/car.png";
  static final String user = baseURL + "global/user.png";

  static final String cardFull = baseURL + "card/card-full.png";
  static final String cardChip = baseURL + "card/chip.png";
  static final String cardThumb = baseURL + "card/card-thumbnail.png";

  static final String notificationIn = baseURL + "notification/in.png";
  static final String notificationOut = baseURL + "notification/out.png";
  static final String notificationLogo = baseURL + "notification/logo.png";

  static final String success = baseURL + "utils/success.png";
  static final String failed = baseURL + "utils/failed.png";
  static final String balance = baseURL + "utils/balance.png";
}

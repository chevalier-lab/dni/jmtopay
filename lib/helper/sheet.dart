import 'package:flutter/cupertino.dart';
import 'package:jmtopay/helper/img.dart';
import 'package:jmtopay/utils/component.dart';

error(BuildContext context, String message) =>
    sheet.build(context, IMG.failed, message);

success(BuildContext context, String message) =>
    sheet.build(context, IMG.success, message);

toastError(BuildContext context, String message) =>
    sheet.build(context, IMG.failed, message);

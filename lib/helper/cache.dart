import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class CacheHelper {
  static save(String label, String data) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.setString("https://jmtopay.my.id//apakah" + label, data);
  }

  static fetch(String label) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString("https://jmtopay.my.id//apakah" + label);
  }

  static remove() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.clear();
  }
}

// Cache Auth
void setAuth(dynamic data) =>
    CacheHelper.save("https://jmtopay.my.id//apakah/auth", jsonEncode(data));
dynamic getAuth() async {
  var temp = await CacheHelper.fetch("https://jmtopay.my.id//apakah/auth");
  if (temp != null)
    return jsonDecode(temp);
  else
    return temp;
}

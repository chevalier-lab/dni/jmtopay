import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:jmtopay/helper/global.dart';
import 'package:jmtopay/helper/url.dart';

class RequestModel {
  final Dio dio;

  RequestModel({@required this.dio});

  // Post Form Data
  void formData(BuildContext context, String uri, var req,
      Function(dynamic) callback, Function(String) callbackErr) async {
    try {
      await models
          .formData(dio, uri, req,
              option: Options(headers: {
                HttpHeaders.contentTypeHeader: "multipart/form-data"
              }))
          .then((value) {
        print("SUCCESS");
        print(value.toString());
        var res = jsonDecode(value.toString());
        callback(res);
      });
    } on DioError catch (e) {
      print(e.error.toString());
      if (e.response != null) {
        print(e.response.toString());
        callbackErr(e.response.toString());
      } else
        callbackErr(e.error.toString());
    }
  }

  // Post Form Data
  void rawData(BuildContext context, String uri, var req,
      Function(dynamic) callback, Function(String) callbackErr) async {
    try {
      await models
          .raw(dio, uri, req,
              option: Options(
                  headers: {HttpHeaders.contentTypeHeader: "application/json"}))
          .then((value) {
        print("SUCCESS");
        print(value.toString());
        var res = jsonDecode(value.toString());
        callback(res);
      });
    } on DioError catch (e) {
      print(e.error.toString());
      if (e.response != null) {
        print(e.response.toString());
        var res = jsonDecode(e.response.toString());
        callbackErr(res[RequestKey.message]);
      } else
        callbackErr(e.error.toString());
    }
  }

  // Post Form Data
  void rawDataFrame(BuildContext context, String uri, var req,
      Function(dynamic) callback, Function(String) callbackErr) async {
    try {
      await models
          .raw(dio, uri, req,
              option: Options(
                  headers: {HttpHeaders.contentTypeHeader: "application/json"}))
          .then((value) {
        print("SUCCESS");
        print(value.toString());
        callback(value.toString());
      });
    } on DioError catch (e) {
      print(e.error.toString());
      if (e.response != null) {
        print(e.response.toString());
        callback(e.response..toString());
      } else
        callbackErr(e.error.toString());
    }
  }

  // Get Data
  void getData(BuildContext context, String uri, Function(dynamic) callback,
      Function(String) callbackErr) async {
    try {
      await models.getData(dio, uri).then((value) {
        print("SUCCESS");
        print(value.toString());
        var res = jsonDecode(value.toString());
        callback(res);
      });
    } on DioError catch (e) {
      print(e.error.toString());
      if (e.response != null) {
        print(e.response.toString());
        callbackErr(e.response.toString());
      } else
        callbackErr(e.error.toString());
    }
  }

  // Form URL Encoded
  void formUrlEncoded(BuildContext context, String uri, var req,
      Function(dynamic) callback, Function(String) callbackErr) async {
    try {
      await models.formURLEncoded(dio, uri, req).then((value) {
        print("SUCCESS");
        print(value.toString());
        var res = jsonDecode(value.toString());
        callback(res);
      });
    } on DioError catch (e) {
      print(e.error.toString());
      if (e.response != null) {
        print(e.response.toString());
        callbackErr(e.response.toString());
      } else
        callbackErr(e.error.toString());
    }
  }

  // Form URL Encoded
  void formUrlEncodedRaw(BuildContext context, String uri, var req,
      Function(dynamic) callback, Function(String) callbackErr) async {
    try {
      await models.formURLEncoded(dio, uri, req).then((value) {
        print("SUCCESS");
        print(value.toString());
        callback(value.toString());
      });
    } on DioError catch (e) {
      print(e.error.toString());
      if (e.response != null) {
        print(e.response.toString());
        callback(e.response.toString());
      } else
        callbackErr(e.error.toString());
    }
  }
}

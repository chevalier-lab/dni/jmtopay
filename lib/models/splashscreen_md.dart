import 'package:flutter/cupertino.dart';
import 'package:jmtopay/helper/img.dart';

class SplashScreenMD extends ChangeNotifier {
  SplashScreenMD();

  List sliders = [
    {
      "ilustration": IMG.welcomeSlider1,
      "title": "Leading Payment\nApplication",
      "description":
          "More than 100 million users with 1,000 thousand partners and services in the world"
    },
    {
      "ilustration": IMG.welcomeSlider2,
      "title": "Prestige and Absolute\nSecurity",
      "description":
          "Licensed by all banks in the world & secured with multi-tier PCI-DSS international standards"
    },
    {
      "ilustration": IMG.welcomeSlider3,
      "title": "Receive Hot Gifts from \nCarPay Right Away",
      "description":
          "Sign up now to receive a large gift pack. Eating, watching movies & many other services."
    }
  ];

  bool isAlreadySetupBefore = false;
  int currentPosition = 0;

  dynamic get getCurrentSlider => this.sliders[this.currentPosition];
  int get getSize => this.sliders.length;
  void setNext() {
    this.currentPosition++;
    notifyListeners();
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jmtopay/helper/img.dart';

class LinkCardMD extends ChangeNotifier {
  LinkCardMD();

  List<dynamic> locals = [
    {"url": IMG.bri, "label": "BRI"},
    {"url": IMG.linkaja, "label": "Link Aja"},
  ];

  List<dynamic> globals = [
    {"url": IMG.paypal, "label": "Paypal"},
    {"url": IMG.visa, "label": "Visa"},
    {"url": IMG.masterCard, "label": "Master Card"},
    {"url": IMG.payoneer, "label": "Payoneer"}
  ];

  dynamic newCard = {
    "icon": IMG.bri,
    "full_name": "",
    "valid_date": "",
    "card_number": ["0000", "0000", "0000", "0000"],
    "balance": 0
  };

  List<dynamic> fieldNewCard = [
    {
      "label": "Card Number",
      "hint": "0000 0000 0000 0000",
      "icon": Icons.input_rounded,
      "type": TextInputType.number,
      "format": FilteringTextInputFormatter.digitsOnly,
      "controller": TextEditingController(text: "")
    },
    {
      "label": "Cardholder Name",
      "hint": "Your Full Name",
      "icon": Icons.person,
      "type": TextInputType.text,
      "format": FilteringTextInputFormatter.singleLineFormatter,
      "controller": TextEditingController(text: "")
    },
    {
      "label": "Month",
      "hint": "00",
      "icon": Icons.calendar_today,
      "type": TextInputType.number,
      "format": FilteringTextInputFormatter.digitsOnly,
      "controller": TextEditingController(text: "")
    },
    {
      "label": "Year",
      "hint": "0000",
      "icon": Icons.calendar_today,
      "type": TextInputType.number,
      "format": FilteringTextInputFormatter.digitsOnly,
      "controller": TextEditingController(text: "")
    }
  ];

  void setCard(dynamic newCard) {
    this.newCard = newCard;
    notifyListeners();
  }

  bool checkCardNumber() =>
      this.fieldNewCard[0]["controller"].text.length == 16;

  void checkSetCardNumber(String value) {
    if (this.fieldNewCard[0]["controller"].text.length > 16) {
      this.fieldNewCard[0]["controller"].text =
          this.fieldNewCard[0]["controller"].text.toString().substring(0, 16);
      notifyListeners();
    } else {
      List<String> newValue = [];
      String tempValue = value;
      for (var i = 0; i < (16 - value.length); i++) {
        tempValue += "0";
      }
      String tempBuffer = "";
      for (var i = 0; i < tempValue.length; i++) {
        if (tempBuffer.length == 4) {
          newValue.add(tempBuffer);
          tempBuffer = "";
        } else
          tempBuffer += tempValue[i];
      }
      this.newCard["card_number"] = newValue;
      notifyListeners();
    }
  }
}

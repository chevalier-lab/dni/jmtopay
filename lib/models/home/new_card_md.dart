import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jmtopay/helper/img.dart';

class NewCardMD extends ChangeNotifier {
  NewCardMD();

  dynamic newCard = {
    "icon": IMG.bri,
    "full_name": "",
    "valid_date": "",
    "card_number": ["0000", "0000", "0000", "0000"],
    "balance": 0
  };

  bool isValid = false;

  List<dynamic> fieldNewCard = [
    {
      "label": "Card Number",
      "hint": "0000 0000 0000 0000",
      "icon": Icons.input_rounded,
      "type": TextInputType.number,
      "max_length": 16,
      "format": FilteringTextInputFormatter.digitsOnly,
      "controller": TextEditingController(text: "")
    },
    {
      "label": "Cardholder Name",
      "hint": "Your Full Name",
      "icon": Icons.person,
      "type": TextInputType.text,
      "max_length": null,
      "format": FilteringTextInputFormatter.singleLineFormatter,
      "controller": TextEditingController(text: "")
    },
    {
      "label": "Month",
      "hint": "00",
      "icon": Icons.calendar_today,
      "type": TextInputType.number,
      "max_length": 2,
      "format": FilteringTextInputFormatter.digitsOnly,
      "controller": TextEditingController(text: "")
    },
    {
      "label": "Year",
      "hint": "00",
      "icon": Icons.calendar_today,
      "type": TextInputType.number,
      "max_length": 2,
      "format": FilteringTextInputFormatter.digitsOnly,
      "controller": TextEditingController(text: "")
    }
  ];

  List<dynamic> fieldPhoneNumber = [
    {
      "label": "Phone Number",
      "hint": "62",
      "icon": Icons.input_rounded,
      "type": TextInputType.number,
      "max_length": 16,
      "format": FilteringTextInputFormatter.digitsOnly,
      "controller": TextEditingController(text: "")
    },
  ];

  void setCard(dynamic newCard) {
    this.newCard = newCard;
    notifyListeners();
  }

  bool checkCardNumber() =>
      this.fieldNewCard[0]["controller"].text.length == 16;

  bool checkFullName() => this.fieldNewCard[1]["controller"].text.length >= 3;

  bool checkMonth() => this.fieldNewCard[2]["controller"].text.length == 2;
  bool checkYear() => this.fieldNewCard[3]["controller"].text.length == 2;

  bool checkPhoneNumber() {
    if (this.fieldPhoneNumber[0]["controller"].text.length >= 1) {
      if (this.fieldPhoneNumber[0]["controller"].text[0] == "0") return false;
    }
    return (this.fieldPhoneNumber[0]["controller"].text.length >= 10 &&
        this.fieldPhoneNumber[0]["controller"].text.length <= 16);
  }

  void checkIsValid() {
    if (checkMonth() && checkYear() && checkCardNumber())
      this.isValid = true;
    else
      this.isValid = false;
    notifyListeners();
  }

  void checkIsValid2() {
    if (checkPhoneNumber())
      this.isValid = true;
    else
      this.isValid = false;
    notifyListeners();
  }

  void setFullName() {
    this.newCard["full_name"] = this.fieldNewCard[1]["controller"].text;
    checkIsValid();
  }

  void setValidDate() {
    this.newCard["valid_date"] = this.fieldNewCard[2]["controller"].text +
        "/" +
        this.fieldNewCard[3]["controller"].text;
    checkIsValid();
  }

  void checkSetCardNumber(String value) {
    if (value.length > 16) {
      this.fieldNewCard[0]["controller"].text = value.substring(0, 16);
      checkIsValid();
    } else {
      List<String> newValue = [];
      String tempBuffer = "";
      for (var i = 0; i < value.length; i++) {
        tempBuffer += value[i];
        if (tempBuffer.length == 4) {
          newValue.add(tempBuffer);
          tempBuffer = "";
        }
      }

      this.newCard["card_number"] = newValue;
      checkIsValid();
    }
  }

  void checkSetPhoneNumber(String value) {
    if (value.length > 16) {
      this.fieldPhoneNumber[0]["controller"].text = value.substring(0, 16);
      checkIsValid2();
    } else {
      List<String> newValue = [];
      String tempBuffer = "";
      for (var i = 0; i < value.length; i++) {
        tempBuffer += value[i];
        if (tempBuffer.length == 4) {
          newValue.add(tempBuffer);
          tempBuffer = "";
        }
      }

      if (value.length < 12 || value.length < 16) {
        newValue.add(value.substring(value.length - (value.length % 4)));
      }

      this.newCard["card_number"] = newValue;
      checkIsValid2();
    }
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jmtopay/helper/img.dart';
import 'package:jmtopay/helper/tint.dart';

class WalletMD extends ChangeNotifier {
  WalletMD();

  dynamic search = {
    "hint": "Search on TollPay",
    "icon": Icons.search,
    "type": TextInputType.text,
    "format": FilteringTextInputFormatter.singleLineFormatter,
    "controller": TextEditingController(text: "")
  };
  bool isValid = false;
  List<dynamic> services = [
    {"url": IMG.info, "label": "Info Toll"},
    {"url": IMG.mobil, "label": "Mobil Derek"},
    {"url": IMG.restArea, "label": "Rest Area"},
    {"url": IMG.cctv, "label": "CCTV"},
    {"url": IMG.topup, "label": "Top Up"},
    {"url": IMG.navigation, "label": "Navigasi"},
    {"url": IMG.spbu, "label": "SPBU Terdekat"}
  ];

  List<dynamic> historyTransactions = [];

  List<dynamic> products = [];

  List<dynamic> profileMenus = [
    {
      "icon": Icons.location_on_outlined,
      "color": Tint.purple,
      "label": "Address",
    },
    {
      "icon": Icons.note,
      "color": Tint.warning,
      "label": "Transaction Histroy",
    },
    {
      "icon": Icons.group,
      "color": Tint.blue,
      "label": "Contract",
    },
    {
      "icon": Icons.settings,
      "color": Tint.field,
      "label": "Setting",
    },
    {
      "icon": Icons.help,
      "color": Tint.field,
      "label": "Help Center",
    },
    {
      "icon": Icons.logout,
      "color": Tint.field,
      "label": "Logout",
    }
  ];

  List<dynamic> notifications = [
    {
      "icon": IMG.notificationIn,
      "label": "Saldo berkurang 16.000",
      "description": "Untuk biaya tol Jakarta out ring road",
      "type": "in"
    },
    {
      "icon": IMG.notificationOut,
      "label": "Kartu debit Anda telah tersambung",
      "description": "Kartu debit BRI telah berhasil tersambung ke akun anda",
      "type": "out"
    },
    {
      "icon": IMG.notificationLogo,
      "label": "Info lalu lintas",
      "description": "Terjadi kemacetan di KM 12 Pasar minggu",
      "type": "info"
    }
  ];

  void setClear() {
    this.search["controller"].text = "";
    this.checkIsValid();
    notifyListeners();
  }

  void setProducts(List<dynamic> items) {
    this.products = items;
    notifyListeners();
  }

  void setTransactionHistory(List<dynamic> items) {
    this.historyTransactions = items;
    notifyListeners();
  }

  void checkIsValid() {
    if (this.search["controller"].text != "")
      this.isValid = true;
    else
      this.isValid = false;
    notifyListeners();
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:jmtopay/apps/home/pages/new_card.dart';
import 'package:jmtopay/apps/home/popups/otp.dart';
import 'package:jmtopay/helper/img.dart';
import 'package:jmtopay/apps/home/pages/wallet.dart';

class HomeMD extends ChangeNotifier {
  HomeMD();

  // String currentPage = NewCardPageProvider.pageName;
  // String currentPage = LinkCardPageProvider.pageName;
  String currentPage = WalletPageProvider.pageName;
  List<dynamic> bottomNavigations = [
    {"icon": Icons.grid_view, "is_center": false, "is_active": true},
    {
      "icon": Icons.notifications_active,
      "is_center": false,
      "is_active": false
    },
    {"icon": Icons.qr_code_rounded, "is_center": true, "is_active": false},
    {
      "icon": Icons.card_giftcard_outlined,
      "is_center": false,
      "is_active": false
    },
    {
      "icon": Icons.person_outline_sharp,
      "is_center": false,
      "is_active": false
    },
  ];

  dynamic currentLocal = {"url": IMG.bri, "label": "BRI"};

  dynamic content = {
    "logo": IMG.logo,
    "title": {
      OTPPage.pageName: {"label": "OTP Authentication"},
    },
    "description": {
      OTPPage.pageName: {"label": "An authentication code has been sent to"},
    },
    "buttons": [
      "Sign In",
      "Create an account",
      "Forgot your credentials?",
      "I didn’t receive code.",
      "Resend Code",
      "By Signing up, you agree to our.",
      "By creating an account, you aggree to our.",
      "Term and Conditions",
      "Sign Up",
      "Already have an account?",
      "Send Code",
      "Next"
    ]
  };

  void setCurrentPage(String currentPage) {
    this.currentPage = currentPage;
    notifyListeners();
  }

  void setCurrentLocal(dynamic currentPage) {
    this.currentLocal = currentPage;
    notifyListeners();
  }

  void setSelectedBottomNavigation(int position) {
    this.bottomNavigations.forEach((element) {
      element["is_active"] = false;
    });
    if (position != 2) this.bottomNavigations[position]["is_active"] = true;
    notifyListeners();
  }
}

import 'package:flutter/cupertino.dart';

class RegistrationMD extends ChangeNotifier {
  RegistrationMD();

  String fieldFullName = "",
      fieldUsername = "",
      fieldEmail = "",
      fieldPhoneNumber = "",
      fieldPassword = "";
  bool isFormValid = false, isAggreeTerms = false;

  void setFullName(String fieldFullName) {
    this.fieldFullName = fieldFullName;
    checkIsFormValid();
  }

  void setPhoneNumber(String fieldPhoneNumber) {
    this.fieldPhoneNumber = fieldPhoneNumber;
    checkIsFormValid();
  }

  void setPassword(String fieldPassword) {
    this.fieldPassword = fieldPassword;
    checkIsFormValid();
  }

  void setEmail(String fieldEmail) {
    this.fieldEmail = fieldEmail;
    checkIsFormValid();
  }

  void setUsername(String fieldUsername) {
    this.fieldUsername = fieldUsername;
    checkIsFormValid();
  }

  void setAgreement(bool isAggreeTerms) {
    this.isAggreeTerms = isAggreeTerms;
    checkIsFormValid();
  }

  void checkIsFormValid() {
    if (this.checkIsFullNameValid() &&
        this.checkIsUsernameValid() &&
        this.checkIsPasswordValid() &&
        this.checkIsEmailValid() &&
        this.checkIsPhoneNumberValid() &&
        this.checkIsAggreement())
      this.isFormValid = true;
    else
      this.isFormValid = false;
    notifyListeners();
  }

  bool checkIsPhoneNumberValid() => (this.fieldPhoneNumber.length >= 10);
  bool checkIsPasswordValid() => (this.fieldPassword.length >= 6);
  bool checkIsFullNameValid() => (this.fieldFullName.length >= 3);
  bool checkIsUsernameValid() => (this.fieldUsername.length >= 6);
  bool checkIsAggreement() => (this.isAggreeTerms == true);
  bool checkIsEmailValid() => (RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
      .hasMatch(this.fieldEmail));
}

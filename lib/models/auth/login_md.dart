import 'package:flutter/cupertino.dart';

class LoginMD extends ChangeNotifier {
  LoginMD();

  String fieldPhoneNumber = "", fieldPassword = "";
  bool isFormValid = false;

  void setPhoneNumber(String fieldPhoneNumber) {
    this.fieldPhoneNumber = fieldPhoneNumber;
    checkIsFormValid();
  }

  void setPassword(String fieldPassword) {
    this.fieldPassword = fieldPassword;
    checkIsFormValid();
  }

  void checkIsFormValid() {
    if (this.checkIsPhoneNumberValid() && this.checkIsPasswordValid())
      this.isFormValid = true;
    else
      this.isFormValid = false;
    notifyListeners();
  }

  bool checkIsPhoneNumberValid() {
    if (this.fieldPhoneNumber.length >= 10) {
      if (this.fieldPhoneNumber[0] == "6" && this.fieldPhoneNumber[1] == "2")
        return true;
    }
    return false;
  }

  bool checkIsPasswordValid() => (this.fieldPassword.length >= 6);
}

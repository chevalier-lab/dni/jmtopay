import 'package:flutter/cupertino.dart';

class PasswordRecoveryMD extends ChangeNotifier {
  PasswordRecoveryMD();

  String fieldPhoneNumber = "";
  bool isFormValid = false;

  void setPhoneNumber(String fieldPhoneNumber) {
    this.fieldPhoneNumber = fieldPhoneNumber;
    checkIsFormValid();
  }

  void checkIsFormValid() {
    if (this.fieldPhoneNumber.length < 10)
      this.isFormValid = false;
    else
      this.isFormValid = true;
    notifyListeners();
  }

  bool checkIsPhoneNumberValid() => (this.fieldPhoneNumber.length >= 10);
}

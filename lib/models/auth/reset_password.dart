import 'package:flutter/cupertino.dart';

class ResetPasswordMD extends ChangeNotifier {
  ResetPasswordMD();

  String fieldNewPassword = "", fieldConfirmPassword = "";
  bool isFormValid = false;

  void setNewPassword(String fieldNewPassword) {
    this.fieldNewPassword = fieldNewPassword;
    checkIsFormValid();
  }

  void setConfirmPassword(String fieldConfirmPassword) {
    this.fieldConfirmPassword = fieldConfirmPassword;
    checkIsFormValid();
  }

  void checkIsFormValid() {
    if (this.fieldNewPassword.length < 6 ||
        this.fieldConfirmPassword.length < 6 ||
        this.fieldNewPassword != this.fieldConfirmPassword)
      this.isFormValid = false;
    else
      this.isFormValid = true;
    notifyListeners();
  }

  bool checkIsNewPasswordValid() => (this.fieldNewPassword.length >= 6);
  bool checkIsConfirmPasswordValid() => (this.fieldConfirmPassword.length >= 6);
}

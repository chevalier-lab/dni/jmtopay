import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class VerifyIdentityMD extends ChangeNotifier {
  VerifyIdentityMD();

  String fieldPIN = "";
  bool isFormValid = false;
  List fields = [
    {
      "type": TextInputType.number,
      "format": FilteringTextInputFormatter.digitsOnly,
      "focusNode": FocusNode(),
      "value": ""
    },
    {
      "type": TextInputType.number,
      "format": FilteringTextInputFormatter.digitsOnly,
      "focusNode": FocusNode(),
      "value": ""
    },
    {
      "type": TextInputType.number,
      "format": FilteringTextInputFormatter.digitsOnly,
      "focusNode": FocusNode(),
      "value": ""
    },
    {
      "type": TextInputType.number,
      "format": FilteringTextInputFormatter.digitsOnly,
      "focusNode": FocusNode(),
      "value": ""
    },
  ];

  void setPIN(String fieldPIN) {
    this.fieldPIN = fieldPIN;
    checkIsFormValid();
  }

  void setFieldsValue(String value, int position) {
    this.fields[position]["value"] = value;
    notifyListeners();
  }

  void checkIsFormValid() {
    if (this.fieldPIN.length != 4)
      this.isFormValid = false;
    else
      this.isFormValid = true;
    notifyListeners();
  }

  bool checkIsPINValid() => (this.fieldPIN.length == 4);
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jmtopay/apps/auth/pages/login.dart';
import 'package:jmtopay/apps/auth/popups/otp.dart';
import 'package:jmtopay/apps/auth/pages/password_recovery.dart';
import 'package:jmtopay/apps/auth/pages/registration.dart';
import 'package:jmtopay/apps/auth/pages/reset_password.dart';
import 'package:jmtopay/apps/auth/pages/verify_identity.dart';
import 'package:jmtopay/helper/img.dart';

class AuthMD extends ChangeNotifier {
  AuthMD();

  String currentPage = LoginPageProvider.pageName;

  dynamic content = {
    "logo": IMG.logo,
    "title": {
      LoginPageProvider.pageName: {"label": "Let’s Sign You In"},
      OTPPage.pageName: {"label": "OTP Authentication"},
      PasswordRecoveryPageProvider.pageName: {"label": "Password Recovery"},
      VerifyIdentityPageProvider.pageName: {"label": "Verify your identity"},
      ResetPasswordPageProvider.pageName: {"label": "Reset your password"},
      RegistrationPageProvider.pageName: {"label": "Getting Started"}
    },
    "description": {
      LoginPageProvider.pageName: {
        "label": "Welcome back, you’ve been missed!"
      },
      OTPPage.pageName: {"label": "An authentication code has been sent to"},
      PasswordRecoveryPageProvider.pageName: {
        "label": "Enter your Phone number to recover your password"
      },
      VerifyIdentityPageProvider.pageName: {
        "label": "We have just sent a code to"
      },
      ResetPasswordPageProvider.pageName: {
        "label": "At least 8 characters, with uppercase and lowercase letters"
      },
      RegistrationPageProvider.pageName: {
        "label": "Create an account to continue!"
      }
    },
    "fields": [
      {
        "label": "Phone Number",
        "hint": "62821456789",
        "icon": Icons.phone_android,
        "type": TextInputType.number,
        "format": FilteringTextInputFormatter.digitsOnly
      },
      {
        "label": "Password",
        "hint": "•••••••••••••",
        "icon": Icons.lock,
        "type": TextInputType.visiblePassword,
        "format": FilteringTextInputFormatter.singleLineFormatter
      },
      {
        "label": "Full Name",
        "hint": "John Doe",
        "icon": Icons.person_outline_rounded,
        "type": TextInputType.text,
        "format": FilteringTextInputFormatter.singleLineFormatter
      },
      {
        "label": "New Password",
        "hint": "•••••••••••••",
        "icon": Icons.lock,
        "type": TextInputType.visiblePassword,
        "format": FilteringTextInputFormatter.singleLineFormatter
      },
      {
        "label": "Confirm Password",
        "hint": "•••••••••••••",
        "icon": Icons.lock,
        "type": TextInputType.text,
        "format": FilteringTextInputFormatter.singleLineFormatter
      },
      {
        "label": "Username",
        "hint": "johnDoe123",
        "icon": Icons.verified_user,
        "type": TextInputType.text,
        "format": FilteringTextInputFormatter.singleLineFormatter
      },
      {
        "label": "Email",
        "hint": "john.doe@email.com",
        "icon": Icons.email,
        "type": TextInputType.emailAddress,
        "format": FilteringTextInputFormatter.singleLineFormatter
      },
    ],
    "buttons": [
      "Sign In",
      "Create an account",
      "Forgot your credentials?",
      "I didn’t receive code.",
      "Resend Code",
      "By Signing up, you agree to our.",
      "By creating an account, you aggree to our.",
      "Term and Conditions",
      "Sign Up",
      "Already have an account?",
      "Send Code",
      "Next"
    ]
  };

  void setCurrentPage(String currentPage) {
    this.currentPage = currentPage;
    notifyListeners();
  }
}
